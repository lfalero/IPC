<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagenCentral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagen_central', function (Blueprint $table) {
            $table->increments('id');           
            $table->string('titulo', 200);
            $table->longText('descripcion');
            $table->string('imagen', 200);
            $table->string('nombre_boton', 100)->nullable();
            $table->string('ruta_boton', 200)->nullable();
            $table->char('estado', 1)->default('A');
            $table->integer('categoria_imagen_central_id')->references('id')->on('categoria_imagen_central');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagen_central');
    }
}
