<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local', function (Blueprint $table) {
            $table->increments('id');           
            $table->string('nombre', 200);
            $table->string('direccion', 200);
            $table->string('dias_culto', 100);
            $table->string('horario_culto', 100);
            $table->string('imagen', 200);
            $table->char('estado', 1)->default('A');
            $table->integer('departamento_id')->references('id')->on('departamento');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local');
    }
}
