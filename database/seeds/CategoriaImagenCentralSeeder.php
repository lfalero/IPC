<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\CategoriaImagenCentral;


class CategoriaImagenCentralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        CategoriaImagenCentral::create(['nombre' => 'Otros']);
        CategoriaImagenCentral::create(['nombre' => 'Bautismos']);
        CategoriaImagenCentral::create(['nombre' => 'Aniversarios']);
        CategoriaImagenCentral::create(['nombre' => 'Campañas']);
        CategoriaImagenCentral::create(['nombre' => 'Coros']);
    }
}