<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        User::create([
        	'dni'=>'71574271',
            'nombres'=>'Luis',
        	'apellidos'=>'Falero Otiniano',
            'correo'=>'lufao1427@gmail.com',
        	'password'=>bcrypt('123123123')
        ]);
    }
}
