<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\CategoriaGaleria;


class CategoriaGaleriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        CategoriaGaleria::create(['nombre' => 'Otros']);
        CategoriaGaleria::create(['nombre' => 'Bautismos']);
        CategoriaGaleria::create(['nombre' => 'Aniversarios']);
        CategoriaGaleria::create(['nombre' => 'Campañas']);
        CategoriaGaleria::create(['nombre' => 'Coros']);
    }
}
