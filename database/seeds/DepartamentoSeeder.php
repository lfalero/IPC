<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Departamento;

class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Departamento::create(['nombre' => 'Amazonas']);
        Departamento::create(['nombre' => 'Áncash']);
        Departamento::create(['nombre' => 'Apurímac']);
        Departamento::create(['nombre' => 'Arequipa']);
        Departamento::create(['nombre' => 'Ayacucho']);
        Departamento::create(['nombre' => 'Cajamarca']);
        Departamento::create(['nombre' => 'Callao']);
        Departamento::create(['nombre' => 'Cuzco']);
        Departamento::create(['nombre' => 'Huancavelica']);
        Departamento::create(['nombre' => 'Huánuco']);
        Departamento::create(['nombre' => 'Ica']);
        Departamento::create(['nombre' => 'Junín']);
        Departamento::create(['nombre' => 'La Libertad']);
        Departamento::create(['nombre' => 'Lambayeque']);
        Departamento::create(['nombre' => 'Lima']);
        Departamento::create(['nombre' => 'Loreto']);
        Departamento::create(['nombre' => 'Madre de Dios']);
        Departamento::create(['nombre' => 'Moquegua']);
        Departamento::create(['nombre' => 'Pasco']);
        Departamento::create(['nombre' => 'Piura']);
        Departamento::create(['nombre' => 'Puno']);
        Departamento::create(['nombre' => 'San Martín']);
        Departamento::create(['nombre' => 'Tacna']);
        Departamento::create(['nombre' => 'Tumbes']);
        Departamento::create(['nombre' => 'Ucayali']);
    }
}




                        
          