<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use DB; 
use Session;
use Validator;
use Input;

use App\Testimonio;

class TestimonioController extends Controller
{
    protected $testimonio;
    
    public function __construct()
    {
        $this->testimonio = new Testimonio();
    }

    public function index()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->testimonio::get();
        $data = array('elementos' => $elementos, 'cantidad' => count($elementos));
        return view('admin.testimonio.listar', $data) ;
    }

    public function create()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        return view('admin.testimonio.registrar');
    }

    public function store(Request $request)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'titulo' => 'required|max:200|unique:testimonio',
            'descripcion' => 'required|max:1500',
            'imagen' => 'required|mimes:jpeg,png,jpg|max:3072',
            'ruta_facebook' => 'max:200',
            'ruta_twitter' => 'max:200',
        ]);

        $imagen =  $request->imagen;
        $extends = $imagen->getClientOriginalExtension();
        //$originalName = $imagen->getClientOriginalName();
        $originalName = $request['titulo'].'.'.$extends;
        
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ"," ");
        $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","_");
        $name = str_replace($no_permitidas, $permitidas ,$originalName);
        
        $ruta = 'images/testimonio';
        $folder = public_path($ruta);
        $imageName = strtolower($name);
        $imagen->move($folder, $imageName);

        $testimonio = new Testimonio();
        $testimonio->titulo = $request['titulo'];
        $testimonio->descripcion = $request['descripcion'];
        $testimonio->imagen =  '/'.$ruta.'/'.$imageName;
        
        if($request['ruta_facebook'] != null){
            $testimonio->ruta_facebook = $request['ruta_facebook'];
        }

        if($request['ruta_twitter'] != null){
            $testimonio->ruta_twitter = $request['ruta_twitter'];
        }
        $testimonio->save();

        Session::flash('flashMessage',config('global.insertMessage')); 
        Session::flash('flashType',config('global.success')); 
        
        if(Input::get('guardar_listar')) {
            return redirect('admin/testimonios'); 
        } elseif(Input::get('guardar')) {
            return redirect('admin/testimonio/nuevo'); 
        }
    }

    public function show($id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->testimonio::where('id', $id)->first();
        $data = array('elementos' => $elementos);
        return view('admin.testimonio.registrar', $data);
    }

    public function update(Request $request, $id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'titulo' => 'required|max:200|unique:testimonio,titulo,'.$id,
            'descripcion' => 'required|max:1500',
            'imagen' => 'mimes:jpeg,png,jpg|max:3072',
            'ruta_facebook' => 'max:200',
            'ruta_twitter' => 'max:200',
        ]);

        $testimonio = $this->testimonio::find($id);

        if($request->imagen != null){
            $imagen =  $request->imagen;
            $extends = $imagen->getClientOriginalExtension();
            //$originalName = $imagen->getClientOriginalName();
            $originalName = $request['titulo'].'.'.$extends;
            
            $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ"," ");
            $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","_");
            $name = str_replace($no_permitidas, $permitidas ,$originalName);
            
            $ruta = 'images/testimonio';
            $folder = public_path($ruta);
            $imageName = strtolower($name);
            $imagen->move($folder, $imageName);
            
            $testimonio->imagen =  '/'.$ruta.'/'.$imageName;
        }
        
        $testimonio->titulo = $request['titulo'];
        $testimonio->descripcion = $request['descripcion'];
        $testimonio->ruta_facebook = $request['ruta_facebook'];
        $testimonio->ruta_twitter = $request['ruta_twitter'];
        
        try {
            $testimonio->save();
            Session::flash('flashMessage',config('global.updateMessage')); 
            Session::flash('flashType',config('global.success'));
            return redirect('admin/testimonios');
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.updateErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/testimonios');           
        }
    }

    public function destroy($id)
    {
        try {
            $testimonio = $this->testimonio::find($id);
            $testimonio->delete();
            Session::flash('flashMessage',config('global.destroyMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/testimonios'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.destroyErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/testimonios');           
        }
    }

    public function habilitar($id)
    {
        try {
            $testimonio = $this->testimonio::find($id);
            $testimonio->estado = 'A';
            $testimonio->save();
            Session::flash('flashMessage',config('global.habilitarMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/testimonios'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.habilitarErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/testimonios');           
        }
    }

    public function deshabilitar($id)
    {
        try {
            $testimonio = $this->testimonio::find($id);
            $testimonio->estado = 'I';
            $testimonio->save();
            Session::flash('flashMessage',config('global.deshabilitarMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/testimonios'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.deshabilitarErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/testimonios');           
        }
    }

}
