<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use DB; 
use Session;
use Validator;
use Input;

use App\Departamento;
use App\Local;

class LocalController extends Controller
{
    protected $departamento, $local;
    
    public function __construct()
    {
        $this->departamento = new Departamento();
        $this->local = new Local();
    }

    public function index()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->local::
        select('local.id','local.nombre','local.dias_culto','local.horario_culto',
        'local.imagen', 'departamento.nombre as departamento')
        ->join('departamento', 'departamento.id', '=', 'local.departamento_id')
        ->orderBy('local.id', 'asc')
        ->get();
        $data = array('elementos' => $elementos, 'cantidad' => count($elementos));
        return view('admin.local.listar', $data) ;
    }

    public function create()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementosDepartamento = $this->departamento::orderBy('nombre')->pluck('nombre', 'id');
        $data = array('elementosDepartamento' => $elementosDepartamento);
        return view('admin.local.registrar', $data);
    }

    public function store(Request $request)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'nombre' => 'required|max:200|unique:local',
            'direccion' => 'required',
            'dias_culto' => 'required|max:100',
            'horario_culto' => 'required|max:100',
            'imagen' => 'required|mimes:jpeg,png,jpg|max:3072',
        ]);

        $imagen =  $request->imagen;
        $extends = $imagen->getClientOriginalExtension();
        //$originalName = $imagen->getClientOriginalName();
        $originalName = $request['nombre'].'.'.$extends;
        
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ"," ",":");
            $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","_","");
        $name = str_replace($no_permitidas, $permitidas ,$originalName);
        
        $ruta = 'images/local';
        $folder = public_path($ruta);
        $imageName = strtolower($name);
        $imagen->move($folder, $imageName);

        $local = new Local();
        $local->departamento_id = $request['departamento_id'];
        $local->nombre = $request['nombre'];
        $local->direccion = $request['direccion'];
        $local->dias_culto = $request['dias_culto'];
        $local->horario_culto = $request['horario_culto'];
        $local->imagen =  '/'.$ruta.'/'.$imageName;
      
        $local->save();

        Session::flash('flashMessage',config('global.insertMessage')); 
        Session::flash('flashType',config('global.success')); 
        
        if(Input::get('guardar_listar')) {
            return redirect('admin/locales'); 
        } elseif(Input::get('guardar')) {
            return redirect('admin/local/nuevo'); 
        }
    }

    public function show($id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->local::where('id', $id)->first();
        $elementosDepartamento = $this->departamento::orderBy('nombre')->pluck('nombre', 'id');
        $data = array('elementos' => $elementos, 'elementosDepartamento' => $elementosDepartamento);
        return view('admin.local.registrar', $data);
    }

    public function update(Request $request, $id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'nombre' => 'required|max:200|unique:local,nombre,'.$id,
            'direccion' => 'required',
            'dias_culto' => 'required|max:100',
            'horario_culto' => 'required|max:100',
            'imagen' => 'mimes:jpeg,png,jpg|max:3072',
        ]);

        $local = $this->local::find($id);

        if($request->imagen != null){
            $imagen =  $request->imagen;
            $extends = $imagen->getClientOriginalExtension();
            //$originalName = $imagen->getClientOriginalName();
            $originalName = $request['nombre'].'.'.$extends;
            
            $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ"," ",":");
            $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","_","");
            $name = str_replace($no_permitidas, $permitidas ,$originalName);
            
            $ruta = 'images/local';
            $folder = public_path($ruta);
            $imageName = strtolower($name);
            $imagen->move($folder, $imageName);
            
            $local->imagen =  '/'.$ruta.'/'.$imageName;
        }
        
        $local->departamento_id = $request['departamento_id'];
        $local->nombre = $request['nombre'];
        $local->direccion = $request['direccion'];
        $local->dias_culto = $request['dias_culto'];
        $local->horario_culto = $request['horario_culto'];
   
        try {
            $local->save();
            Session::flash('flashMessage',config('global.updateMessage')); 
            Session::flash('flashType',config('global.success'));
            return redirect('admin/locales');
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.updateErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/locales');           
        }
    }

    public function destroy($id)
    {
        try {
            $local = $this->local::find($id);
            $local->delete();
            Session::flash('flashMessage',config('global.destroyMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/locales'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.destroyErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/locales');           
        }
    }
}
