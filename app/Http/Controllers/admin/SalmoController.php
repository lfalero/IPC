<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use DB; 
use Session;
use Validator;
use Input;

use App\Salmo;

class SalmoController extends Controller
{
    protected $salmo;
    
    public function __construct()
    {
        $this->salmo = new Salmo();
    }

    public function index()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->salmo::get();
        $data = array('elementos' => $elementos, 'cantidad' => count($elementos));
        return view('admin.salmo.listar', $data) ;
    }

    public function create()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        return view('admin.salmo.registrar');
    }

    public function store(Request $request)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'nombre' => 'required|unique:salmo',
        ]);

        $salmo = new Salmo();
        $salmo->nombre = $request['nombre'];
        $salmo->save();

        Session::flash('flashMessage',config('global.insertMessage')); 
        Session::flash('flashType',config('global.success')); 
        
        if(Input::get('guardar_listar')) {
            return redirect('admin/salmos'); 
        } elseif(Input::get('guardar')) {
            return redirect('admin/salmo/nuevo'); 
        }
    }

    public function show($id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->salmo::where('id', $id)->first();
        $data = array('elementos' => $elementos);
        return view('admin.salmo.registrar', $data);
    }

    public function update(Request $request, $id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'nombre' => 'required|unique:salmo,nombre,'.$id,
        ]);

        $salmo = $this->salmo::find($id);
        $salmo->nombre = $request['nombre'];
   
        try {
            $salmo->save();
            Session::flash('flashMessage',config('global.updateMessage')); 
            Session::flash('flashType',config('global.success'));
            return redirect('admin/salmos');
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.updateErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/salmos');           
        }
    }

    public function destroy($id)
    {
        try {
            $salmo = $this->salmo::find($id);
            $salmo->delete();
            Session::flash('flashMessage',config('global.destroyMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/salmos'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.destroyErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/salmos');           
        }
    }

    public function habilitar($id)
    {
        try {
            $salmo = $this->salmo::find($id);
            $salmo->estado = 'A';
            $salmo->save();
            Session::flash('flashMessage',config('global.habilitarMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/salmos'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.habilitarErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/salmos');           
        }
    }

    public function deshabilitar($id)
    {
        try {
            $salmo = $this->salmo::find($id);
            $salmo->estado = 'I';
            $salmo->save();
            Session::flash('flashMessage',config('global.deshabilitarMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/salmos'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.deshabilitarErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/salmos');           
        }
    }

}
