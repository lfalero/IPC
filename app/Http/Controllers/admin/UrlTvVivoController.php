<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use DB; 
use Session;
use Validator;
use Input;

use App\UrlTvVivo;

class UrlTvVivoController extends Controller
{
    protected $url_tv_vivo;
    
    public function __construct()
    {
        $this->url_tv_vivo = new UrlTvVivo();
    }

    public function index()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementosUrl = $this->url_tv_vivo::orderBy('url')->pluck('url', 'id');
        $elementoActual = $this->url_tv_vivo::where('estado', 'A')->first();

        $elementos = $this->url_tv_vivo::get();
        $data = array('elementos' => $elementos, 'elementosUrl' => $elementosUrl,
        'elementoActual' => $elementoActual, 'cantidad' => count($elementos));
        return view('admin.url-tv-vivo.listar', $data) ;
    }

    public function create()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        return view('admin.url-tv-vivo.registrar');
    }

    public function store(Request $request)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'titulo' => 'required|unique:url_tv_vivo',
            'url' => 'required|unique:url_tv_vivo',
        ]);

        $url_tv_vivo = new UrlTvVivo();
        $url_tv_vivo->titulo = $request['titulo'];
        $url_tv_vivo->url = $request['url'];
        $url_tv_vivo->save();

        Session::flash('flashMessage',config('global.insertMessage')); 
        Session::flash('flashType',config('global.success')); 
        
        if(Input::get('guardar_listar')) {
            return redirect('admin/url-tv-vivos'); 
        } elseif(Input::get('guardar')) {
            return redirect('admin/url-tv-vivo/nuevo'); 
        }
    }

    public function show($id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->url_tv_vivo::where('id', $id)->first();
        $data = array('elementos' => $elementos);
        return view('admin.url-tv-vivo.registrar', $data);
    }

    public function update(Request $request, $id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'titulo' => 'required|unique:url_tv_vivo,titulo,'.$id,
            'url' => 'required|unique:url_tv_vivo,url,'.$id,
        ]);

        $url_tv_vivo = $this->url_tv_vivo::find($id);
        $url_tv_vivo->titulo = $request['titulo'];
        $url_tv_vivo->url = $request['url'];
   
        try {
            $url_tv_vivo->save();
            Session::flash('flashMessage',config('global.updateMessage')); 
            Session::flash('flashType',config('global.success'));
            return redirect('admin/url-tv-vivos');
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.updateErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/url-tv-vivos');           
        }
    }

    public function destroy($id)
    {
        try {
            $url_tv_vivo = $this->url_tv_vivo::find($id);
            $url_tv_vivo->delete();
            Session::flash('flashMessage',config('global.destroyMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/url-tv-vivos'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.destroyErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/url-tv-vivos');           
        }
    }

    public function habilitar($id)
    {
        try {
            $this->url_tv_vivo::query()->update(['estado' => 'I']);
            
            $url_tv_vivo = $this->url_tv_vivo::find($id);
            $url_tv_vivo->estado = 'A';
            $url_tv_vivo->save();
            Session::flash('flashMessage',config('global.habilitarMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/url-tv-vivos'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.habilitarErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/url-tv-vivos');           
        }
    }
}
