<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Image;
use DB; 
use Session;
use Validator;
use Input;

use App\Anuncio;

class AnuncioController extends Controller
{
    protected $anuncio;
    
    public function __construct()
    {
        $this->anuncio = new Anuncio();
    }

    public function index()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->anuncio::get();
        $data = array('elementos' => $elementos, 'cantidad' => count($elementos));
        return view('admin.anuncio.listar', $data) ;
    }

    public function create()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        return view('admin.anuncio.registrar');
    }

    public function store(Request $request)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }
        
        $this->validate($request, [
            'titulo' => 'required|max:200|unique:anuncio',
            'descripcion' => 'required',
            'imagen' => 'required|mimes:jpeg,png,jpg|max:3072|dimensions:max_width=1000,max_width=1000',
            'ruta_facebook' => 'max:200',
            'ruta_twitter' => 'max:200',
        ]);

        $imagen =  $request->imagen;
        
        $extends = $imagen->getClientOriginalExtension();
        //$originalName = $imagen->getClientOriginalName();
        $originalName = $request['titulo'].'.'.$extends;
        
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ"," ",":");
        $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","_","");
        $name = str_replace($no_permitidas, $permitidas ,$originalName);
        
        $ruta = 'images/anuncio';
        $folder = public_path($ruta);      
        $imageName = strtolower($name);
        $width = 275;
        $height = 310;

        //TAMAÑO IMAGEN
        $path  = $request->file('imagen');
        $file = $folder.'/'.$imageName;
        Image::make($path)->fit($width, $height)->save($file);        
        //$imagen->move($folder, $imageName);

        $anuncio = new Anuncio();
        $anuncio->titulo = $request['titulo'];
        $anuncio->descripcion = $request['descripcion'];
        $anuncio->imagen =  '/'.$ruta.'/'.$imageName;
        
        if($request['ruta_facebook'] != null){
            $anuncio->ruth6a_facebook = $request['ruta_facebook'];
        }

        if($request['ruta_twitter'] != null){
            $anuncio->ruta_twitter = $request['ruta_twitter'];
        }
        $anuncio->save();

        Session::flash('flashMessage',config('global.insertMessage')); 
        Session::flash('flashType',config('global.success')); 
        
        if(Input::get('guardar_listar')) {
            return redirect('admin/anuncios'); 
        } elseif(Input::get('guardar')) {
            return redirect('admin/anuncio/nuevo'); 
        }
    }

    public function show($id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->anuncio::where('id', $id)->first();
        $data = array('elementos' => $elementos);
        return view('admin.anuncio.registrar', $data);
    }

    public function update(Request $request, $id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'titulo' => 'required|max:200|unique:anuncio,titulo,'.$id,
            'descripcion' => 'required',
            'imagen' => 'mimes:jpeg,png,jpg|max:3072',
            'ruta_facebook' => 'max:200',
            'ruta_twitter' => 'max:200',
        ]);

        $anuncio = $this->anuncio::find($id);

        if($request->imagen != null){
            $imagen =  $request->imagen;
            $extends = $imagen->getClientOriginalExtension();
            //$originalName = $imagen->getClientOriginalName();
            $originalName = $request['titulo'].'.'.$extends;
            
            $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ"," ",":");
            $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","_","");
            $name = str_replace($no_permitidas, $permitidas ,$originalName);
            
            $ruta = 'images/anuncio';
            $folder = public_path($ruta);
            $imageName = strtolower($name);
            $width = 275;
            $height = 310;

            //TAMAÑO IMAGEN
            $path  = $request->file('imagen');
            $file = $folder.'/'.$imageName;
            Image::make($path)->fit($width, $height)->save($file);        
            //$imagen->move($folder, $imageName);
            
            $anuncio->imagen =  '/'.$ruta.'/'.$imageName;
        }
        
        $anuncio->titulo = $request['titulo'];
        $anuncio->descripcion = $request['descripcion'];
        $anuncio->ruta_facebook = $request['ruta_facebook'];
        $anuncio->ruta_twitter = $request['ruta_twitter'];
   
        try {
            $anuncio->save();
            Session::flash('flashMessage',config('global.updateMessage')); 
            Session::flash('flashType',config('global.success'));
            return redirect('admin/anuncios');
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.updateErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/anuncios');           
        }
    }

    public function destroy($id)
    {
        try {
            $anuncio = $this->anuncio::find($id);
            $anuncio->delete();
            Session::flash('flashMessage',config('global.destroyMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/anuncios'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.destroyErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/anuncios');           
        }
    }

    public function habilitar($id)
    {
        try {
            $anuncio = $this->anuncio::find($id);
            $anuncio->estado = 'A';
            $anuncio->save();
            Session::flash('flashMessage',config('global.habilitarMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/anuncios'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.habilitarErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/anuncios');           
        }
    }

    public function deshabilitar($id)
    {
        try {
            $anuncio = $this->anuncio::find($id);
            $anuncio->estado = 'I';
            $anuncio->save();
            Session::flash('flashMessage',config('global.deshabilitarMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/anuncios'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.deshabilitarErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/anuncios');           
        }
    }
}
