<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use DB; 
use Session;
use Validator;
use Input;

use App\User;

class UserController extends Controller
{
    protected $user;
    
    public function __construct()
    {
        $this->user = new User();
    }

    public function list()
    {        
        if(isset(Auth::user()->id)){
            return view('admin.public.inicio');
        }else{
            return view('admin.login');
        }
    }

    public function login(Request $request)
    {   
        $this->validate($request, [
            'correo' => 'required|email|max:255|exists:users',
            'clave' => 'required|max:255'
        ]);

        $correo = $request->correo;
        $clave = $request->clave;

        if(Auth::attempt(array('correo'=>$correo, 'password'=>$clave))){
            return redirect('admin');
        }else{
            return redirect()->back()
            ->withInput()
            ->withErrors([
                'correo' => 'Estas credenciales no coinciden con nuestros registros.',
            ]);
        }
    }

    public function signOff()
    {   
        Session::flush();
        return redirect('admin');
    }
}
