<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use DB; 
use Session;
use Validator;
use Input;

use App\Noticia;

class NoticiaController extends Controller
{
    protected $noticia;
    
    public function __construct()
    {
        $this->noticia = new Noticia();
    }

    public function index()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->noticia::get();
        $data = array('elementos' => $elementos, 'cantidad' => count($elementos));
        return view('admin.noticia.listar', $data) ;
    }

    public function create()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        return view('admin.noticia.registrar');
    }

    public function store(Request $request)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'titulo' => 'required|max:200|unique:noticia',
            'descripcion' => 'required|max:1500',
            'imagen' => 'required|mimes:jpeg,png,jpg|max:3072',
            'ruta_facebook' => 'max:200',
            'ruta_twitter' => 'max:200',
        ]);

        $imagen =  $request->imagen;
        $extends = $imagen->getClientOriginalExtension();
        //$originalName = $imagen->getClientOriginalName();
        $originalName = $request['titulo'].'.'.$extends;
        
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ"," ");
        $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","_");
        $name = str_replace($no_permitidas, $permitidas ,$originalName);
        
        $ruta = 'images/noticia';
        $folder = public_path($ruta);
        $imageName = strtolower($name);
        $imagen->move($folder, $imageName);

        $noticia = new Noticia();
        $noticia->titulo = $request['titulo'];
        $noticia->descripcion = $request['descripcion'];
        $noticia->imagen =  '/'.$ruta.'/'.$imageName;
        
        if($request['ruta_facebook'] != null){
            $noticia->ruta_facebook = $request['ruta_facebook'];
        }

        if($request['ruta_twitter'] != null){
            $noticia->ruta_twitter = $request['ruta_twitter'];
        }
        $noticia->save();

        Session::flash('flashMessage',config('global.insertMessage')); 
        Session::flash('flashType',config('global.success')); 
        
        if(Input::get('guardar_listar')) {
            return redirect('admin/noticias'); 
        } elseif(Input::get('guardar')) {
            return redirect('admin/noticia/nuevo'); 
        }
    }

    public function show($id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->noticia::where('id', $id)->first();
        $data = array('elementos' => $elementos);
        return view('admin.noticia.registrar', $data);
    }

    public function update(Request $request, $id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'titulo' => 'required|max:200|unique:noticia,titulo,'.$id,
            'descripcion' => 'required|max:1500',
            'imagen' => 'mimes:jpeg,png,jpg|max:3072',
            'ruta_facebook' => 'max:200',
            'ruta_twitter' => 'max:200',
        ]);

        $noticia = $this->noticia::find($id);

        if($request->imagen != null){
            $imagen =  $request->imagen;
            $extends = $imagen->getClientOriginalExtension();
            //$originalName = $imagen->getClientOriginalName();
            $originalName = $request['titulo'].'.'.$extends;
            
            $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ"," ");
            $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","_");
            $name = str_replace($no_permitidas, $permitidas ,$originalName);
            
            $ruta = 'images/noticia';
            $folder = public_path($ruta);
            $imageName = strtolower($name);
            $imagen->move($folder, $imageName);
            
            $noticia->imagen =  '/'.$ruta.'/'.$imageName;
        }
        
        $noticia->titulo = $request['titulo'];
        $noticia->descripcion = $request['descripcion'];
        $noticia->ruta_facebook = $request['ruta_facebook'];
        $noticia->ruta_twitter = $request['ruta_twitter'];
        
        try {
            $noticia->save();
            Session::flash('flashMessage',config('global.updateMessage')); 
            Session::flash('flashType',config('global.success'));
            return redirect('admin/noticias');
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.updateErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/noticias');           
        }
    }

    public function destroy($id)
    {
        try {
            $noticia = $this->noticia::find($id);
            $noticia->delete();
            Session::flash('flashMessage',config('global.destroyMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/noticias'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.destroyErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/noticias');           
        }
    }

    public function habilitar($id)
    {
        try {
            $noticia = $this->noticia::find($id);
            $noticia->estado = 'A';
            $noticia->save();
            Session::flash('flashMessage',config('global.habilitarMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/noticias'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.habilitarErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/noticias');           
        }
    }

    public function deshabilitar($id)
    {
        try {
            $noticia = $this->noticia::find($id);
            $noticia->estado = 'I';
            $noticia->save();
            Session::flash('flashMessage',config('global.deshabilitarMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/noticias'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.deshabilitarErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/noticias');           
        }
    }

}
