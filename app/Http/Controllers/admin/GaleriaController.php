<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use DB; 
use Session;
use Validator;
use Input;
use ImageOptimizer;

use App\CategoriaGaleria;
use App\Galeria;

class GaleriaController extends Controller
{
    protected $galeria;
    
    public function __construct()
    {
        $this->categoria_galeria = new CategoriaGaleria();
        $this->galeria = new Galeria();
    }

    public function index()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->galeria::
        select('galeria.id','galeria.titulo','galeria.estado', 'galeria.created_at',
        'galeria.imagen', 'categoria_galeria.nombre as categoria_galeria')
        ->join('categoria_galeria', 'categoria_galeria.id', '=', 'galeria.categoria_galeria_id')
        ->orderBy('galeria.id', 'asc')
        ->get();
        $data = array('elementos' => $elementos, 'cantidad' => count($elementos));
        return view('admin.galeria.listar', $data) ;
    }

    public function create()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementosCategoriaGaleria = $this->categoria_galeria::orderBy('nombre')->pluck('nombre', 'id');
        $data = array('elementosCategoriaGaleria' => $elementosCategoriaGaleria);
        return view('admin.galeria.registrar', $data);
    }

    public function store(Request $request)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'titulo' => 'required|max:200|unique:galeria',
            'imagen' => 'required|mimes:jpeg,png,jpg|max:3072',
        ]);

        $imagen =  $request->imagen;
        $extends = $imagen->getClientOriginalExtension();
        //$originalName = $imagen->getClientOriginalName();
        $originalName = $request['titulo'].'.'.$extends;
        
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ"," ",":");
        $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","_","");
        $name = str_replace($no_permitidas, $permitidas ,$originalName);
        
        $ruta = 'images/galeria';
        $folder = public_path($ruta);
        $imageName = strtolower($name);
        $imagen->move($folder, $imageName);

        $galeria = new Galeria();
        $galeria->categoria_galeria_id = $request['categoria_galeria_id'];
        $galeria->titulo = $request['titulo'];
        $galeria->imagen =  '/'.$ruta.'/'.$imageName;
       
        $galeria->save();        

        Session::flash('flashMessage',config('global.insertMessage')); 
        Session::flash('flashType',config('global.success')); 
        
        if(Input::get('guardar_listar')) {
            return redirect('admin/galerias'); 
        } elseif(Input::get('guardar')) {
            return redirect('admin/galeria/nuevo'); 
        }

        /*$this->validate($request, [
            'imagen' => 'required|image|mimes:jpeg,jpg,png,bmp,gif,svg|max:8192',
        ]);
        $pathToImage = 'images/galeria/demo.jpg';        
        ImageOptimizer::optimize($request->imagen, $pathToImage);*/
        //upload_max_filesize = 1000M;
        //post_max_size = 1000M;
    }

    public function show($id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->galeria::where('id', $id)->first();
        $elementosCategoriaGaleria = $this->categoria_galeria::orderBy('nombre')->pluck('nombre', 'id');
        $data = array('elementos' => $elementos, 'elementosCategoriaGaleria' => $elementosCategoriaGaleria);
        return view('admin.galeria.registrar', $data);
    }

    public function update(Request $request, $id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'titulo' => 'required|max:200|unique:galeria,titulo,'.$id,
            'imagen' => 'mimes:jpeg,png,jpg|max:3072',
        ]);

        $galeria = $this->galeria::find($id);

        if($request->imagen != null){
            $imagen =  $request->imagen;
            $extends = $imagen->getClientOriginalExtension();
            //$originalName = $imagen->getClientOriginalName();
            $originalName = $request['titulo'].'.'.$extends;
            
            $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ"," ",":");
            $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","_","");
            $name = str_replace($no_permitidas, $permitidas ,$originalName);
            
            $ruta = 'images/galeria';
            $folder = public_path($ruta);
            $imageName = strtolower($name);
            $imagen->move($folder, $imageName);
            
            $galeria->imagen =  '/'.$ruta.'/'.$imageName;
        }
        
        $galeria->categoria_galeria_id = $request['categoria_galeria_id'];
        $galeria->titulo = $request['titulo'];
   
        try {
            $galeria->save();
            Session::flash('flashMessage',config('global.updateMessage')); 
            Session::flash('flashType',config('global.success'));
            return redirect('admin/galerias');
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.updateErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/galerias');           
        }
    }

    public function destroy($id)
    {
        try {
            $galeria = $this->galeria::find($id);
            $galeria->delete();
            Session::flash('flashMessage',config('global.destroyMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/galerias'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.destroyErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/galerias');           
        }
    }

    public function habilitar($id)
    {
        try {
            $galeria = $this->galeria::find($id);
            $galeria->estado = 'A';
            $galeria->save();
            Session::flash('flashMessage',config('global.habilitarMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/galerias'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.habilitarErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/galerias');           
        }
    }

    public function deshabilitar($id)
    {
        try {
            $galeria = $this->galeria::find($id);
            $galeria->estado = 'I';
            $galeria->save();
            Session::flash('flashMessage',config('global.deshabilitarMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/galerias'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.deshabilitarErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/galerias');           
        }
    }

}
