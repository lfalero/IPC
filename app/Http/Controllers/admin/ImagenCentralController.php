<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use DB; 
use Session;
use Validator;
use Input;

use App\CategoriaImagenCentral;
use App\ImagenCentral;

class ImagenCentralController extends Controller
{
    protected $imagen_central;
    
    public function __construct()
    {
        $this->categoria_imagen_central = new CategoriaImagenCentral();
        $this->imagen_central = new ImagenCentral();
    }

    public function index()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->imagen_central::
        select('imagen_central.id','imagen_central.titulo','imagen_central.descripcion','imagen_central.estado',
        'imagen_central.imagen', 'categoria_imagen_central.nombre as categoria_imagen_central')
        ->join('categoria_imagen_central', 'categoria_imagen_central.id', '=', 'imagen_central.categoria_imagen_central_id')
        ->orderBy('imagen_central.id', 'asc')
        ->get();
        $data = array('elementos' => $elementos, 'cantidad' => count($elementos));
        return view('admin.imagen-central.listar', $data) ;
    }

    public function create()
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementosCategoriaImagenCentral = $this->categoria_imagen_central::orderBy('nombre')->pluck('nombre', 'id');
        $data = array('elementosCategoriaImagenCentral' => $elementosCategoriaImagenCentral);
        return view('admin.imagen-central.registrar', $data);
    }

    public function store(Request $request)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'titulo' => 'required|max:200|unique:imagen_central',
            'descripcion' => 'required',
            'imagen' => 'required|mimes:jpeg,png,jpg|max:3072',
            'nombre_boton' => 'max:100',
            'ruta_boton' => 'max:200'
        ]);

        $imagen =  $request->imagen;
        $extends = $imagen->getClientOriginalExtension();
        //$originalName = $imagen->getClientOriginalName();
        $originalName = $request['titulo'].'.'.$extends;
        
        $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ"," ",":");
        $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","_","");
        $name = str_replace($no_permitidas, $permitidas ,$originalName);
        
        $ruta = 'images/imagen_central';
        $folder = public_path($ruta);
        $imageName = strtolower($name);
        $imagen->move($folder, $imageName);

        $imagen_central = new ImagenCentral();
        $imagen_central->categoria_imagen_central_id = $request['categoria_imagen_central_id'];
        $imagen_central->titulo = $request['titulo'];
        $imagen_central->descripcion = $request['descripcion'];
        $imagen_central->imagen =  '/'.$ruta.'/'.$imageName;
        
        if($request['nombre_boton'] != null){
            $imagen_central->nombre_boton = $request['nombre_boton'];
        }
        if($request['ruta_boton'] != null){
            $imagen_central->ruta_boton = $request['ruta_boton'];
        }
        $imagen_central->save();

        Session::flash('flashMessage',config('global.insertMessage')); 
        Session::flash('flashType',config('global.success')); 
        
        if(Input::get('guardar_listar')) {
            return redirect('admin/imagenes-centrales'); 
        } elseif(Input::get('guardar')) {
            return redirect('admin/imagen-central/nuevo'); 
        }
    }

    public function show($id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $elementos = $this->imagen_central::where('id', $id)->first();
        $elementosCategoriaImagenCentral = $this->categoria_imagen_central::orderBy('nombre')->pluck('nombre', 'id');
        $data = array('elementos' => $elementos, 'elementosCategoriaImagenCentral' => $elementosCategoriaImagenCentral);
        return view('admin.imagen-central.registrar', $data);
    }

    public function update(Request $request, $id)
    {
        if(!isset(Auth::user()->id)){
            return redirect('/');
        }

        $this->validate($request, [
            'titulo' => 'required|max:200|unique:imagen_central,titulo,'.$id,
            'descripcion' => 'required',
            'imagen' => 'mimes:jpeg,png,jpg|max:3072',
            'nombre_boton' => 'max:100',
            'ruta_boton' => 'max:200',
        ]);

        $imagen_central = $this->imagen_central::find($id);

        if($request->imagen != null){
            $imagen =  $request->imagen;
            $extends = $imagen->getClientOriginalExtension();
            //$originalName = $imagen->getClientOriginalName();
            $originalName = $request['titulo'].'.'.$extends;
            
            $no_permitidas= array ("á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","Ñ"," ",":");
            $permitidas= array ("a","e","i","o","u","A","E","I","O","U","n","N","_","");
            $name = str_replace($no_permitidas, $permitidas ,$originalName);
            
            $ruta = 'images/imagen_central';
            $folder = public_path($ruta);
            $imageName = strtolower($name);
            $imagen->move($folder, $imageName);
            
            $imagen_central->imagen =  '/'.$ruta.'/'.$imageName;
        }
        
        $imagen_central->categoria_imagen_central_id = $request['categoria_imagen_central_id'];
        $imagen_central->titulo = $request['titulo'];
        $imagen_central->descripcion = $request['descripcion'];
        $imagen_central->nombre_boton = $request['nombre_boton'];
        $imagen_central->ruta_boton = $request['ruta_boton'];
   
        try {
            $imagen_central->save();
            Session::flash('flashMessage',config('global.updateMessage')); 
            Session::flash('flashType',config('global.success'));
            return redirect('admin/imagenes-centrales');
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.updateErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/imagenes-centrales');           
        }
    }

    public function destroy($id)
    {
        try {
            $imagen_central = $this->imagen_central::find($id);
            $imagen_central->delete();
            Session::flash('flashMessage',config('global.destroyMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/imagenes-centrales'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.destroyErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/imagenes-centrales');           
        }
    }

    public function habilitar($id)
    {
        try {
            $imagen_central = $this->imagen_central::find($id);
            $imagen_central->estado = 'A';
            $imagen_central->save();
            Session::flash('flashMessage',config('global.habilitarMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/imagenes-centrales'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.habilitarErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/imagenes-centrales');           
        }
    }

    public function deshabilitar($id)
    {
        try {
            $imagen_central = $this->imagen_central::find($id);
            $imagen_central->estado = 'I';
            $imagen_central->save();
            Session::flash('flashMessage',config('global.deshabilitarMessage')); 
            Session::flash('flashType',config('global.success')); 
            return redirect('admin/imagenes-centrales'); 
        } catch (QueryException $e) {
            Session::flash('flashMessage',config('global.deshabilitarErrorMessage')); 
            Session::flash('flashType',config('global.danger')); 
            return redirect('admin/imagenes-centrales');           
        }
    }

}
