<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use DB; 
use Session;
use Validator;
use Input;

use App\CategoriaGaleria;
use App\Galeria;
use App\CategoriaImagenCentral;
use App\ImagenCentral;
use App\Noticia;
use App\Anuncio;
use App\Local;
use App\Departamento;
use App\Salmo;
use App\Testimonio;
use App\UrlTvVivo;

class PaginaController extends Controller
{
    protected $galeria;
    
    public function __construct()
    {
        $this->categoria_galeria = new CategoriaGaleria();
        $this->galeria = new Galeria();
        $this->categoria_imagen_central = new CategoriaImagenCentral();
        $this->imagen_central = new ImagenCentral();
        $this->noticia = new Noticia();
        $this->anuncio = new Anuncio();
        $this->testimonio = new Testimonio();
        $this->local = new Local();
        $this->departamento = new Departamento();
        $this->salmo = new Salmo();
        $this->url_tv_vivo = new UrlTvVivo();
    }

    public function listadoMusica($url){
        //$url = "http://iplacosecha.pe/audiosipc";
        //$url =  "http://www.cvsoluciones.com/archivosIPC/MUSICA/MUSICA%20ECUATORIANA/ALBAZOS%20ECUATORIANO";
        $entities = array('/', '%20', '%c3%91', '%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
        $replacements = array('', ' ', 'Ã‘', '!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
        $array = [];
        $retorno = [];

        $website = file_get_contents($url); // download contents of www.example.com
        preg_match_all("<a href=\x22(.+?)\x22>", $website, $matches); // save all links \x22 = "

        // delete redundant parts
        $matches = str_replace("a href=", "", $matches); // remove a href=
        $matches = str_replace("\"", "", $matches); // remove "
        $array = $matches[1];

        for($i=1; $i<count($array); $i++){
            $string = $array[$i];
            $retorno[$i-1] = str_replace($entities, $replacements, $string);
        }
        //return $retorno;
        return $retorno;
        //return json_encode($retorno);
    }

    public function index()
    {
        $elementosImagenCentral = $this->imagen_central::where('estado', 'A')->orderBy('id', 'asc')->get();
        
        $elementosDepartamento = $this->local::select('departamento.id', 'departamento.nombre')
        ->join('departamento', 'departamento.id', '=', 'local.departamento_id')
        ->where('local.estado', 'A')->where('departamento.estado', 'A')
        ->orderBy('departamento.id', 'asc')->distinct()->get();
        for($i=0; $i<count($elementosDepartamento); $i++){
            $elementosLocal = $this->local::select('local.id', 'local.nombre')
            ->join('departamento', 'departamento.id', '=', 'local.departamento_id')
            ->where('local.estado', 'A')->where('departamento.estado', 'A')
            ->where('local.departamento_id', $elementosDepartamento[$i]["id"])
            ->orderBy('local.id', 'asc')->distinct()->get();

            $elementosDepartamento[$i]["local"] = $elementosLocal;
        }
        
        #%d-%b-%Y
        $elementosNoticia = $this->noticia::select('id', 'titulo', 'descripcion', 'imagen',
        'ruta_facebook', 'ruta_twitter',
        DB::raw("DATE_FORMAT(created_at, '%d') as dia"),
        DB::raw("DATE_FORMAT(created_at, '%b') as mes"))
        ->where('estado', 'A')->orderBy('id', 'desc')->get();
        
        $elementosAnuncio = $this->anuncio::select('id', 'titulo', 'descripcion', 'imagen', 
        'ruta_facebook', 'ruta_twitter',
        DB::raw("DATE_FORMAT(created_at, '%d') as dia"),
        DB::raw("DATE_FORMAT(created_at, '%b') as mes"))
        ->where('estado', 'A')->orderBy('id', 'desc')->get();

        $elementosTestimonio = $this->testimonio::select('id', 'titulo', 'descripcion', 'imagen', 
        'ruta_facebook', 'ruta_twitter',
        DB::raw("DATE_FORMAT(created_at, '%d') as dia"),
        DB::raw("DATE_FORMAT(created_at, '%b') as mes"))
        ->where('estado', 'A')->orderBy('id', 'desc')->get();

        $elementosSalmo = $this->salmo::where('estado', 'A')->orderBy('id', 'desc')->first();

        $elementoUrlTvVivo = $this->url_tv_vivo::where('estado', 'A')->first();

        $data = array('elementosImagenCentral' => $elementosImagenCentral, 'elementosNoticia' => $elementosNoticia,
        'elementosAnuncio' => $elementosAnuncio, 'elementosDepartamento' => $elementosDepartamento, 
        'elementosSalmo' => $elementosSalmo, 'elementosTestimonio' => $elementosTestimonio, 
        'elementoUrlTvVivo' => $elementoUrlTvVivo);
        
        return view('web.principal.inicio', $data);
    }

    public function detalleLocal(Request $request){
        $id = $request['id'];
        $elementos = $this->local::select('local.id', 'local.nombre', 'local.direccion',
        'local.dias_culto', 'local.horario_culto', 'local.imagen')
        ->join('departamento', 'departamento.id', '=', 'local.departamento_id')
        ->where('local.estado', 'A')->where('departamento.estado', 'A')
        ->where('local.id', $id)->first();
        $retorno = false;

        if($request->ajax()){
            $retorno = true;

            if(count($elementos) > 0){
                return response()->json([
                    'mensaje'=>  $retorno,
                    'elementos'=>  $elementos,
                    'cantidad' => count($elementos)
                ]);
            }else{
                 return response()->json([
                    'mensaje'=>  $retorno,
                    'cantidad' => count($elementos)
                ]);
            }
        }else{
            return response()->json([
                'mensaje'=>  $retorno
            ]);
        }
    }

    public function detalleNoticia(Request $request){
        $id = $request['id'];
        $elementos = $this->noticia::where('id', $id)->first();
        $retorno = false;

        if($request->ajax()){
            $retorno = true;
            if(count($elementos) > 0){
                return response()->json([
                    'mensaje'=>  $retorno,
                    'elementos'=>  $elementos,
                    'cantidad' => count($elementos)
                ]);
            }else{
                 return response()->json([
                    'mensaje'=>  $retorno,
                    'cantidad' => count($elementos)
                ]);
            }
        }else{
            return response()->json([
                'mensaje'=>  $retorno
            ]);
        }
    }

    public function detalleAnuncio(Request $request){
        $id = $request['id'];
        $elementos = $this->anuncio::where('id', $id)->first();
        $retorno = false;

        if($request->ajax()){
            $retorno = true;
            if(count($elementos) > 0){
                return response()->json([
                    'mensaje'=>  $retorno,
                    'elementos'=>  $elementos,
                    'cantidad' => count($elementos)
                ]);
            }else{
                 return response()->json([
                    'mensaje'=>  $retorno,
                    'cantidad' => count($elementos)
                ]);
            }
        }else{
            return response()->json([
                'mensaje'=>  $retorno
            ]);
        }
    }

    public function detalleTestimonio(Request $request){
        $id = $request['id'];
        $elementos = $this->testimonio::where('id', $id)->first();
        $retorno = false;

        if($request->ajax()){
            $retorno = true;
            if(count($elementos) > 0){
                return response()->json([
                    'mensaje'=>  $retorno,
                    'elementos'=>  $elementos,
                    'cantidad' => count($elementos)
                ]);
            }else{
                 return response()->json([
                    'mensaje'=>  $retorno,
                    'cantidad' => count($elementos)
                ]);
            }
        }else{
            return response()->json([
                'mensaje'=>  $retorno
            ]);
        }
    }    

    public function detalleMusica(Request $request){
        $elementos = $this->listadoMusica($request['url']);
        $retorno = false;

        if($request->ajax()){
            $retorno = true;
            if(count($elementos) > 0){
                return response()->json([
                    'mensaje'=>  $retorno,
                    'elementos'=>  $elementos,
                    'cantidad' => count($elementos)
                ]);
            }else{
                 return response()->json([
                    'mensaje'=>  $retorno,
                    'cantidad' => count($elementos)
                ]);
            }
        }else{
            return response()->json([
                'mensaje'=>  $retorno
            ]);
        }
    }  

    public function detalleCategoriaGaleria(Request $request){
        $elementos = $this->categoria_galeria::where('estado', 'A')->orderBy('id', 'desc')->get();
        $retorno = false;

        if($request->ajax()){
            $retorno = true;
            if(count($elementos) > 0){
                return response()->json([
                    'mensaje'=>  $retorno,
                    'elementos'=>  $elementos,
                    'cantidad' => count($elementos)
                ]);
            }else{
                 return response()->json([
                    'mensaje'=>  $retorno,
                    'cantidad' => count($elementos)
                ]);
            }
        }else{
            return response()->json([
                'mensaje'=>  $retorno
            ]);
        }
    }  

    public function detalleGaleria(Request $request){
        $elementos = $this->galeria::where('estado', 'A')
        ->where('categoria_galeria_id', $request['categoria_galeria_id'])
        ->orderBy('id', 'desc')->get();
        $retorno = false;

        if($request->ajax()){
            $retorno = true;
            if(count($elementos) > 0){
                return response()->json([
                    'mensaje'=>  $retorno,
                    'elementos'=>  $elementos,
                    'cantidad' => count($elementos)
                ]);
            }else{
                 return response()->json([
                    'mensaje'=>  $retorno,
                    'cantidad' => count($elementos)
                ]);
            }
        }else{
            return response()->json([
                'mensaje'=>  $retorno
            ]);
        }
    }  

    public function correoPedidoOracion(Request $request){
        $nombres = $request['nombres'];
        $correo = $request['correo'];
        $motivo = $request['motivo'];
        $pais = $request['pais'];
        $descripcion = $request['descripcion'];

        $data = array("nombre_correo" => config('global.MAIL_TO_NAME'), 'nombres' => $nombres, 
        'correo' => $correo, 'motivo' => $motivo, 'pais' => $pais, 'descripcion' => $descripcion);  

        if($request->ajax()){
            Mail::send('web.correo.pedido-oracion', $data, function ($message) {            
                $message->from(config('global.MAIL_FROM_ADDRESS'), config('global.titleSite'))      
                ->to(config('global.MAIL_TO_ADDRESS'), config('global.MAIL_TO_NAME'))
                ->subject(config('global.SUBCJECT_PEDIDO_ORACION'));
            });
            return response()->json(['mensaje'=> true]);
        }else{
            return response()->json(['mensaje'=> false]);
        }
    }

    public function correoEscribenos(Request $request){
        $nombres = $request['nombres'];
        $correo = $request['correo'];
        $telefono = $request['telefono'];
        $descripcion = $request['descripcion'];

        $data = array("nombre_correo" => config('global.MAIL_TO_NAME'), 'nombres' => $nombres, 
        'correo' => $correo, 'telefono' => $telefono, 'descripcion' => $descripcion);  

        if($request->ajax()){
            Mail::send('web.correo.escribenos', $data, function ($message) {            
                $message->from(config('global.MAIL_FROM_ADDRESS'), config('global.titleSite'))      
                ->to(config('global.MAIL_TO_ADDRESS'), config('global.MAIL_TO_NAME'))
                ->subject(config('global.SUBCJECT_PEDIDO_CONTACTENOS'));
            });
            return response()->json(['mensaje'=> true]);
        }else{
            return response()->json(['mensaje'=> false]);
        }
    }

    public function correoTestimonio(Request $request){
        $nombres = $request['nombres'];
        $correo = $request['correo'];
        $telefono = $request['telefono'];
        $descripcion = $request['descripcion'];

        $data = array("nombre_correo" => config('global.MAIL_TO_NAME'), 'nombres' => $nombres, 
        'correo' => $correo, 'telefono' => $telefono, 'descripcion' => $descripcion);  

        if($request->ajax()){
            Mail::send('web.correo.escribenos', $data, function ($message) {            
                $message->from(config('global.MAIL_FROM_ADDRESS'), config('global.titleSite'))      
                ->to(config('global.MAIL_TO_ADDRESS'), config('global.MAIL_TO_NAME'))
                ->subject(config('global.SUBCJECT_PEDIDO_TESTIMONIOS'));
            });
            return response()->json(['mensaje'=> true]);
        }else{
            return response()->json(['mensaje'=> false]);
        }
    }
}
