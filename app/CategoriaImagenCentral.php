<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaImagenCentral extends Model
{
    protected $table = 'categoria_imagen_central';
    protected $guarded = ['id'];
}
