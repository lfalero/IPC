<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UrlTvVivo extends Model
{
    protected $table = 'url_tv_vivo';
    protected $guarded = ['id'];
}
