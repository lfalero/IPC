<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaGaleria extends Model
{
    protected $table = 'categoria_galeria';
    protected $guarded = ['id'];
}
