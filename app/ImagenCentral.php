<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenCentral extends Model
{
    protected $table = 'imagen_central';
    protected $guarded = ['id'];
}
