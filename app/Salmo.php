<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salmo extends Model
{
    protected $table = 'salmo';
    protected $guarded = ['id'];
}
