<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::group(['namespace' => 'web'], function () {
    Route::get('/', 'PaginaController@index');
    Route::get('/demo', 'PaginaController@listadoMusica');
    Route::post('principal/ruta_mostrar_detalle_local', 'PaginaController@detalleLocal');
    Route::post('principal/ruta_mostrar_detalle_noticia', 'PaginaController@detalleNoticia');
    Route::post('principal/ruta_mostrar_detalle_anuncio', 'PaginaController@detalleAnuncio');
    Route::post('principal/ruta_mostrar_detalle_testimonio', 'PaginaController@detalleTestimonio');
    Route::post('principal/ruta_mostrar_detalle_carpeta_musica', 'PaginaController@detalleMusica');
    Route::post('principal/ruta_mostrar_detalle_musica', 'PaginaController@detalleMusica');
    Route::post('principal/ruta_mostrar_detalle_carpeta_galeria', 'PaginaController@detalleCategoriaGaleria');
    Route::post('principal/ruta_mostrar_detalle_galeria', 'PaginaController@detalleGaleria');
    Route::post('principal/ruta_correo_pedido_oracion', 'PaginaController@correoPedidoOracion');
    Route::post('principal/ruta_correo_escribenos', 'PaginaController@correoEscribenos');
    Route::post('principal/ruta_correo_testimonios', 'PaginaController@correoTestimonios');
});

Route::group(['namespace' => 'admin'], function () {
    Route::post('usuario/login', 'UserController@login');
    Route::get('admin', 'UserController@list');
    Route::get('admin/signOff', 'UserController@signOff');

    Route::get('admin/galeria/nuevo', 'GaleriaController@create');
    Route::get('admin/galerias', 'GaleriaController@index');
	Route::get('admin/galeria/{id}', 'GaleriaController@show');
    Route::post('galeria/registrar', 'GaleriaController@store');
    //Route::post('galeria/registrar', 'GaleriaController@store')->middleware('optimizeImages');
	Route::post('galeria/modificar/{id}', 'GaleriaController@update');
    Route::post('galeria/eliminar/{id}', 'GaleriaController@destroy');
    Route::post('galeria/habilitar/{id}', 'GaleriaController@habilitar');
    Route::post('galeria/deshabilitar/{id}', 'GaleriaController@deshabilitar');

    Route::get('admin/url-tv-vivo/nuevo', 'UrlTvVivoController@create');
    Route::get('admin/url-tv-vivos', 'UrlTvVivoController@index');
	Route::get('admin/url-tv-vivo/{id}', 'UrlTvVivoController@show');
	Route::post('url-tv-vivo/registrar', 'UrlTvVivoController@store');
	Route::post('url-tv-vivo/modificar/{id}', 'UrlTvVivoController@update');
    Route::post('url-tv-vivo/eliminar/{id}', 'UrlTvVivoController@destroy');
    Route::post('url-tv-vivo/habilitar/{id}', 'UrlTvVivoController@habilitar');

    Route::get('admin/imagen-central/nuevo', 'ImagenCentralController@create');
    Route::get('admin/imagenes-centrales', 'ImagenCentralController@index');
	Route::get('admin/imagen-central/{id}', 'ImagenCentralController@show');
	Route::post('imagen-central/registrar', 'ImagenCentralController@store');
	Route::post('imagen-central/modificar/{id}', 'ImagenCentralController@update');
    Route::post('imagen-central/eliminar/{id}', 'ImagenCentralController@destroy');
    Route::post('imagen-central/habilitar/{id}', 'ImagenCentralController@habilitar');
    Route::post('imagen-central/deshabilitar/{id}', 'ImagenCentralController@deshabilitar');

    Route::get('admin/local/nuevo', 'LocalController@create');
    Route::get('admin/locales', 'LocalController@index');
	Route::get('admin/local/{id}', 'LocalController@show');
	Route::post('local/registrar', 'LocalController@store');
	Route::post('local/modificar/{id}', 'LocalController@update');
    Route::post('local/eliminar/{id}', 'LocalController@destroy');

    Route::get('admin/anuncio/nuevo', 'AnuncioController@create');
    Route::get('admin/anuncios', 'AnuncioController@index');
	Route::get('admin/anuncio/{id}', 'AnuncioController@show');
	Route::post('anuncio/registrar', 'AnuncioController@store');
	Route::post('anuncio/modificar/{id}', 'AnuncioController@update');
    Route::post('anuncio/eliminar/{id}', 'AnuncioController@destroy');
    Route::post('anuncio/habilitar/{id}', 'AnuncioController@habilitar');
    Route::post('anuncio/deshabilitar/{id}', 'AnuncioController@deshabilitar');

    Route::get('admin/noticia/nuevo', 'NoticiaController@create');
    Route::get('admin/noticias', 'NoticiaController@index');
	Route::get('admin/noticia/{id}', 'NoticiaController@show');
	Route::post('noticia/registrar', 'NoticiaController@store');
	Route::post('noticia/modificar/{id}', 'NoticiaController@update');
    Route::post('noticia/eliminar/{id}', 'NoticiaController@destroy');
    Route::post('noticia/habilitar/{id}', 'NoticiaController@habilitar');
    Route::post('noticia/deshabilitar/{id}', 'NoticiaController@deshabilitar');

    Route::get('admin/salmo/nuevo', 'SalmoController@create');
    Route::get('admin/salmos', 'SalmoController@index');
	Route::get('admin/salmo/{id}', 'SalmoController@show');
	Route::post('salmo/registrar', 'SalmoController@store');
	Route::post('salmo/modificar/{id}', 'SalmoController@update');
    Route::post('salmo/eliminar/{id}', 'SalmoController@destroy');
    Route::post('salmo/habilitar/{id}', 'SalmoController@habilitar');
    Route::post('salmo/deshabilitar/{id}', 'SalmoController@deshabilitar');

    Route::get('admin/testimonio/nuevo', 'TestimonioController@create');
    Route::get('admin/testimonios', 'TestimonioController@index');
	Route::get('admin/testimonio/{id}', 'TestimonioController@show');
	Route::post('testimonio/registrar', 'TestimonioController@store');
	Route::post('testimonio/modificar/{id}', 'TestimonioController@update');
    Route::post('testimonio/eliminar/{id}', 'TestimonioController@destroy');
    Route::post('testimonio/habilitar/{id}', 'TestimonioController@habilitar');
    Route::post('testimonio/deshabilitar/{id}', 'TestimonioController@deshabilitar');
});
