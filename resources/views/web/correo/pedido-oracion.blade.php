
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%"
style="background-color:#f8f8f8;font-family:&quot;Open Sans&quot;,&quot;Helvetica&quot;,&quot;Arial&quot;,sans-serif;">
<tbody>
 <tr>
  <td valign="top" style="padding-top:20px;padding-bottom:20px" align="center">
   <table cellpadding="0" cellspacing="0" width="700" style="border:1px solid #f2f2f2; border-radius:5px">
    <tbody>
     <!-- LOGO -->
     <tr>
      <td valign="top" style="background-color:#fff;border-bottom:1px solid #f2f2f2;width: 100%;">
       <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
         <tr>
          <td class="" valign="middle" align="left">
           <a href="#">
            <img class="logo" src="{{ asset('images/logo.png') }}" 
            width="300" height="60" alt="{{ config('global.titleSite') }}"
            style="padding: 10px 0 10px 20px;">
           </a>
          </td>
         </tr>
        </tbody>
       </table>
      </td>
     </tr>
     <!-- END LOGO -->

     <!-- CONTENIDO -->
     <tr style="background-color:#ffffff">
      <td valign="top" style="padding:10px 5px 10px 5px">
       <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
         <td width="23" class="sidespace">&nbsp;</td>
          <td>
           <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class="inner" id="banner" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
            <tr>
             <td style="font-weight:300;font-size:17px;font-family:Open Sans,Arial,Helvetica,sans-serif;color:#333332" 
             class="smallfont">Hola {{ $nombre_correo }},</td>
            </tr>
            <tr>
             <td height="20">&nbsp;</td>
            </tr>
           </table>
          </td>
          <td width="23" class="sidespace">&nbsp;</td>
        </tr>
       </table>
       <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
         <td width="23" class="sidespace">&nbsp;</td>
         <td>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="left" class="inner">
            <tr>
              <td style="font-weight:300;font-size:17px;font-family: Open Sans, Arial, Helvetica, sans-serif; color:#333332;" class="smallfont">
                  Se registró la siguiente información:
                  <br><br>
                  <span style="font-weight: bold;">Nombre: </span>{{ $nombres }}<br>
                  <span style="font-weight: bold;">Correo Electrónico: </span>{{ $correo }}<br>
                  <span style="font-weight: bold;">Motivo: </span>{{ $motivo }}<br>
                  <span style="font-weight: bold;">Pais: </span>{{ $pais }}<br>
                  <span style="font-weight: bold;">Descripción: </span>{{ $descripcion }}<br>
                  <br>
              </td>
            </tr>
            <tr>
                <td height="20">&nbsp;</td>
                <td class="hide" height="20">&nbsp;</td>
            </tr>
            <tr>
            </tr>
          </table>
          <!--
          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="right" class="inner" 
          style="margin-top:10px">
           <tr>
            <td width="23" class="x_sidespace">&nbsp;</td>
            <td style="font:13px/17px Arial, Helvetica, sans-serif; color:#555;">Si necesita ayuda, por favor pónganse en contacto con nosotros, al correo <b> {{ config('global.MAIL_CONTACT') }}. </b></td>
           </tr>
           <tr>
            <td height="20">&nbsp;</td>
           </tr>
          </table>
          -->
         </td>
         <td width="23" class="sidespace">&nbsp;</td>
        </tr>
        <tr>
         <td height="16">&nbsp;</td>
         <td height="16">&nbsp;</td>
         <td height="16">&nbsp;</td>
        </tr>
       </table>

       <!-- DERECHOS RESERVADOS -->
       <table border="0" cellpadding="0" cellspacing="0" width="100%" 
       style="border-top:1px solid #f2f2f2; padding-top:15px;">
        <tbody>
         <tr>
          <td class="" valign="middle" align="center">
           <span style="font-size:12px;color:#3e3e3e">
            <p>© Derechos de autor {{ date('Y') }} | 
             <a href="http://www.iplacosecha.pe">
             {{ config('global.titleSite') }}</a>. Todos los derechos reservados
            </p>
           </span>
          </td>
         </tr>
        </tbody>
       </table>
       <!-- END DERECHOS RESERVADOS -->

      </td>
     </tr>
    </tbody>
   </table>
  </td>
 </tr>
</tbody>
</table>