<!DOCTYPE html>
<html lang="es">
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>IGLESIA PENTECOSTAL LA COSECHA</title>
		<link rel="shortcut icon" type="image/x-icon" href="/web/assets/image/util/favicon.png">
		<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700%7CRoboto:400,500,700%7COpen+Sans:400,600,700" rel="stylesheet" type="text/css">
		<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
		<link type="text/css" rel="stylesheet" href="/web/assets/css/ionicons.css" />
		<link type="text/css" rel="stylesheet" href="/web/assets/css/bootstrap.min.css" />
		<link type="text/css" rel="stylesheet" href="/web/assets/css/owl.carousel.css">
		<link href="/web/assets/css/settings.css" type="text/css" rel="stylesheet" media="screen">
		<link href="/web/assets/css/layers.css" type="text/css" rel="stylesheet" media="screen">
		<link type="text/css" rel="stylesheet" href="/web/assets/css/style.css" />
		<link type="text/css" rel="stylesheet" href="/web/assets/css/index.css" />
		<link type="text/css" rel="stylesheet" href="/web/assets/css/header.css" />
		<link type="text/css" rel="stylesheet" href="/web/assets/css/footer.css" />
		<link type="text/css" rel="stylesheet" href="/web/assets/css/musica.css" />
		<link type="text/css" rel="stylesheet" href="/web/assets/css/galeria.css" />

		<link media="all" type="text/css" rel="stylesheet" href="/administrador/assets/notify/css/icomoon.min.css">
		<link media="all" type="text/css" rel="stylesheet" href="/administrador/assets/notify/css/titatoggle.min.css">
		<script type="text/javascript" src="/web/assets/js/jquery.min.js"></script>
		<script type="text/javascript" src="/administrador/assets/notify/js/bootstrap-notify.min.js"></script>
		<!-- https://www.google.com/recaptcha/admin#site/340406772-->
		<!--<script src='https://www.google.com/recaptcha/api.js'></script>-->
	</head>
	<body>

		<!--loader-->
		<div id="preloader">
			<div class="sk-circle">
				<div class="sk-circle1 sk-child"></div>
				<div class="sk-circle2 sk-child"></div>
				<div class="sk-circle3 sk-child"></div>
				<div class="sk-circle4 sk-child"></div>
				<div class="sk-circle5 sk-child"></div>
				<div class="sk-circle6 sk-child"></div>
				<div class="sk-circle7 sk-child"></div>
				<div class="sk-circle8 sk-child"></div>
				<div class="sk-circle9 sk-child"></div>
				<div class="sk-circle10 sk-child"></div>
				<div class="sk-circle11 sk-child"></div>
				<div class="sk-circle12 sk-child"></div>
			</div>
		</div>
		<!--loader-->

		<!-- header -->
		<header id="inicio">
			<input type="hidden" name="_token" id="token_mostrar_detalle_local" value = "{{ csrf_token() }}">
			<input type="hidden" name="_token" id="token_mostrar_detalle_noticia" value = "{{ csrf_token() }}">
			<input type="hidden" name="_token" id="token_mostrar_detalle_anuncio" value = "{{ csrf_token() }}">
			<input type="hidden" name="_token" id="token_mostrar_detalle_testimonio" value = "{{ csrf_token() }}">
			<input type="hidden" name="_token" id="token_correo_pedido_oracion" value = "{{ csrf_token() }}">
			<input type="hidden" name="_token" id="token_correo_escribenos" value = "{{ csrf_token() }}">
			<input type="hidden" name="_token" id="token_correo_testimonio" value = "{{ csrf_token() }}">
			<input type="hidden" name="_token" id="token_mostrar_detalle_carpeta_musica" value = "{{ csrf_token() }}">
			<input type="hidden" name="_token" id="token_mostrar_detalle_musica" value = "{{ csrf_token() }}">
			<input type="hidden" name="_token" id="token_mostrar_detalle_carpeta_galeria" value = "{{ csrf_token() }}">
			<input type="hidden" name="_token" id="token_mostrar_detalle_galeria" value = "{{ csrf_token() }}">

			<div class="middel-part__block">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 text-center">
							<img src="/web/assets/image/util/logo.png" class="logo">
						</div>
						<div class="col-lg-6">
							<div class="top-info__block text-right">
								<ul>
									<li>
										<i class="fa fa-map-marker"></i>
										<p>
											Sede Nacional  
											<span>Calle Juan Fanning 457</span>
											<span>CHICLAYO PERÚ</span>
										</p>
									</li>
									<li>
										<i class="fa fa-clock-o"></i>
										<p>
											Correo 
											<span>info@iplacosecha.pe</span>
										</p>
									</li>
									<li>
										<i class="fa fa-phone"></i>
										<p>
											Teléfono <span> 074 225814</span>
										</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- counter -->
			<div class="container">
				<div class="row">
					<div class="col-md-8 absol-count offset-md-2">
						<div class="row">
							<div class="col-sm-12 col-md-12 text-center counter-section__box">
								@if($elementosSalmo !=  null)
								<MARQUEE>{{$elementosSalmo->nombre}}</MARQUEE>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="main_nav stricky-header__top">
				<nav class="navbar navbar-toggleable-md navbar-sticky bootsnav">
					<div class="container">
						<div class="navbar-header clearfix">
							<button class="navbar-toggler navbar-toggler-right" type="button" id="menu_toggler">
								<i class="fa fa-bars"></i>
							</button>
						</div>
						<div class="collapse navbar-collapse mobile__nav header-navigation-div">
							<ul class="nav navbar-nav mobile-menu mobile__nav-Ul header-navigation-ul">
								<li>
									<a href="#inicio" class="link_scroll_inicio">INICIO</a>
								</li>
								<li>
									<a href="#biografia" class="link_scroll_biografia">BIOGRAFÍA</a>
								</li>
								<li>
									<a href="#sedes" class="link_scroll_sedes">SEDES IPC</a>
								</li>
								<li>
									<a href="#anuncios" class="link_scroll_anuncios">CAMPAÑAS</a>
								</li>
								<li>
									<a href="#contactenos" class="link_scroll_contactenos">CONTÁCTENOS</a>
								</li>
							</ul>

						</div>
					</div>
				</nav>
			</div>
		</header>

		<!-- Maine-Banner -->
		<div class="main-banner banner_up">
			<div id="rev_slider_34_1_wrapper" class="rev_slider_wrapper" data-alias="news-gallery34">
				<div id="rev_slider_34_1" class="rev_slider" data-version="5.0.7">
					<ul>
						@foreach ($elementosImagenCentral as $index => $elementosImagenCentral)  
						<li data-index="rs-{{$elementosImagenCentral->id}}"  >
							<!-- MAIN IMAGE -->

							{{ Html::image($elementosImagenCentral->imagen, $elementosImagenCentral->titulo, array('class' => 'rev-slidebg')) }}
							<!-- LAYERS -->
							<!-- LAYER NR. 2 -->
							<div class="tp-caption Newspaper-Title tp-resizeme "
							id="slide-129-layer-1"
							data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
							data-y="['center','center','center','center']" data-voffset="['0','0','0','50']"
							data-fontsize="['50','50','50','30']"
							data-lineheight="['55','55','55','35']"
							data-width="['600','600','600','420']"
							data-height="none"
							data-whitespace="normal"
							data-transform_idle="o:1;"
							data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
							data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
							data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
							data-mask_out="x:0;y:0;s:inherit;e:inherit;"
							data-start="1000"
							data-splitin="none"
							data-splitout="none"
							data-responsive_offset="on" >
								<div class="banner-text text-center">
									<h2>{!! $elementosImagenCentral->titulo!!}</h2>
									<p>{!! strip_tags($elementosImagenCentral->descripcion)!!}</p>
									@if($elementosImagenCentral->nombre_boton != null)
									<a class="btn-text" target="_blanck" href="{{$elementosImagenCentral->ruta_boton}}">{{$elementosImagenCentral->nombre_boton}}</a>
									@endif
								</div>
							</div>
						</li>
						@endforeach 
					</ul>
					<div class="tp-bannertimer tp-bottom"></div>
				</div>
			</div>
		</div>

		<div class="container" style="padding-top: 40px;">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-12 text-center" style="margin-bottom: 25px;">
							<p class="letras-banco">USTED QUE SIENTE EN SU CORAZON HACER UNA OFRENDA PARA LA OBRA DE DIOS, PUEDE DEPOSITAR SU OFRENDA O PRIMICIA A CUALQUIERA DE NUESTRAS CUENTAS</p>
						</div>
						<div class="col-md-4 div-pago">
							<img class="imagen-monto" src="/web/assets/image/util/banco-interbank.png" width="90" height="31" align="middle">
							&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="texto-monto">EN SOLES S/. </span>
							<span class="texto-cuenta">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;327-3075406112</span>
						</div>
						<div class="col-md-4 div-pago">
							<img class="imagen-monto" src="/web/assets/image/util/banco-bcp.png" width="90" height="31" align="middle">
							&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="texto-monto">EN SOLES S/. </span>
							<span class="texto-cuenta">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;305-1897613-0-14</span>
						</div>
						<div class="col-md-4 div-pago">
							<img class="imagen-monto" src="/web/assets/image/util/banco-bcp.png" width="90" height="31" align="middle">
							&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="texto-monto">EN DOLARES $ </span>
							<span class="texto-cuenta">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;305-2280518-1-51</span>
						</div>
					</div>					
				</div>
			</div>
		</div>

		<!-- about-us -->
		<div id="galeria" class="ptb-xs-40" style="padding-top: 50px; padding-bottom: 90px;">
			<div class="container">
				<div class="row mb-60 mb-xs-30 text-center">
					<div class="col-md-12">
						<h2>Galería Multimedia</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-1 mb-30 mb-xs-30">
					</div>
					<div class="col-md-2 mb-30 mb-xs-30">
						<!--<a href="http://media.cadenaradiovision.pe:8080/music/login.view;jsessionid=1d8kvh5c7hrb5?" target="_blank">-->
						<a onclick="verDatalleCarpetaMusica(0)">
							<div class="serv-poin text-center servies-point">
								<div class="icon-serv">
									<i class="fa fa-microphone" aria-hidden="true"></i>
								</div>
								<div class="categrie mt-20"><h3>AUDIOS</h3></div>
							</div>
						</a>
					</div>
					<div class="col-md-2 mb-30 mb-xs-30" onclick="verDatalleCarpetaGaleria(0)">
						<div class="serv-poin text-center servies-point">
							<div class="icon-serv">
								<i class="fa fa-camera-retro" aria-hidden="true"></i>
							</div>
							<div class="categrie mt-20"><h3>FOTOS</h3></div>
						</div>
					</div>
					<div class="col-md-2 mb-30 mb-xs-30">
						<a href="https://www.youtube.com/channel/UCOIkzQPfM5A0KnQcTyICgxQ" target="_blank">
							<div class="serv-poin text-center servies-point">
								<div class="icon-serv">
									<i class="fa fa-film" aria-hidden="true"></i>
								</div>
								<div class="categrie mt-20"><h3>VIDEOS</h3></div>
							</div>
						</a>
					</div>
					<div class="col-md-2 mb-30 mb-xs-30">	
						@if(isset($elementoUrlTvVivo))
							<a href="{{"http://".$elementoUrlTvVivo['url']}}" target="_blank">
						@else
							<a>
						@endif
							<div class="serv-poin text-center servies-point">
								<div class="icon-serv">
									<i class="fa fa-desktop" aria-hidden="true"></i>
								</div>
								<div class="categrie mt-20"><h3>TV EN VIVO</h3></div>
							</div>
						</a>					
					</div>
					<div class="col-md-2 mb-30 mb-xs-30">
						<a href="http://cadenaradiovision.pe/multiplataformaradial.html"  target="_blank">
							<div class="serv-poin text-center servies-point">
								<div class="icon-serv">
									<i class="fa fa-bullhorn" aria-hidden="true"></i>
								</div>
								<div class="categrie mt-20"><h3>RADIO EN VIVO</h3></div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>

		<!-- Biografia -->
		<div class="div_biografia">
		<section id="biografia" class="overlay-dark80 text-center dark-bg padding ptb-xs-40">
			<div class="container">

				<div class="breadcrumb-box" style="margin-bottom: 20px; margin-top: -50px;">
					<div class="container">
						<nav aria-label="breadcrumb" role="navigation">
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
									<a id="fundador" class="color-contactanos cursor-pointer" onclick="biografia(0)">
										<font style="vertical-align: inherit;">
											<font style="vertical-align: inherit;">
												Biografía del Fundador y Presidente 
											</font>
										</font>
									</a>
								</li>
								<li class="breadcrumb-item active" aria-current="page">
									<a id="resena"  onclick="biografia(1)" class="cursor-pointer">
										<font style="vertical-align: inherit;">
											<font style="vertical-align: inherit;">
												Reseña Histórica IPC
											</font>
										</font>
									</a>
								</li>
							</ol>
						</nav>
					</div>
				</div>

				<div class="row" id="div_fundador">
					<div class="col-lg-6 col-md-12 ptb-40 testimorial-bg">
						<div class="nf-carousel-theme white">
							<div class="item">
								<div class="testimonial dark-color">
									<h6 class="quote-author text-center">
										BIOGRAFIA DEL PASTOR FRANCISCO CORDOVA RODRIGUEZ
									</h6>
									<span class="biografia lead text-justify">
										<br>
										<p>Nació: el 18 de octubre de 1947 en Piura.</p>
										<p>Se convirtió al Evangelio de nuestro Señor Jesucristo en setiembre del año 1988 en la ciudad de Lima. Perteneció a las Asambleas de Dios, también perteneció a la Iglesia Pentecostal Dios es Amor de 1990 al 1992.</p>
									</span>
									<div class="boton-view-biografia">
										<a class="link-modal" data-toggle="modal" data-target="#modalBiografia">Ver Mas</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row none" id="div_resena">
					<div class="col-lg-6 col-md-12 ptb-40 testimorial-bg">
						<div class="nf-carousel-theme white">
							<div class="item">
								<div class="testimonial dark-color">
									<h6 class="quote-author text-center">
										 RESEÑA HISTÓRICA IPC
									</h6>
									<span class="biografia lead text-justify">
										<br>
										<p>IGLESIA PENTECOSTAL LA COSECHA Fue fundado el 18 de Diciembre de 1992 por revelación del Espíritu Santo cuando el Pastor: Francisco Córdova Rodríguez, Presidente y Fundador de este Ministerio Cristiano, estaba en el país de Panamá, predicando la palabra de Dios.</p>
									</span>
									<div class="boton-view-biografia">
										<a class="link-modal" data-toggle="modal" data-target="#modalResena">Ver Mas</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		</div>
		<!-- End Testimonials -->
	
		<!-- Modal -->
		<div class="modal fade" id="modalBiografia" tabindex="-1" role="dialog" aria-labelledby="modalBiografiaTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<h5 class="modal-title" id="modalBiografiaTitle">BIOGRAFIA DEL PASTOR FRANCISCO CORDOVA RODRIGUEZ</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>
				<div class="modal-body text-justify modal-body-scroll">
					<div>
						<img src="/web/assets/image/util/FRANCISCO-CORDOVA-RODRIGUEZ.jpg" hspace="5" vspace="5" class="imagen-pastor"/>
						<p class="letra-modal">Nació: el 18 de octubre de 1947 en Piura.</p>
						<p class="letra-modal">Se convirtió al Evangelio de nuestro Señor Jesucristo en setiembre del año 1988 en la ciudad de Lima. Perteneció a las Asambleas de Dios, también perteneció a la Iglesia Pentecostal Dios es Amor de 1990 al 1992.</p>
						<p class="letra-modal">Cita bíblica Hechos Cap. 16 vers. 6 al 10. regresando al Perú el Espíritu Santo reveló este ministerio que se llama IGLESIA PENTECOSTAL LA COSECHA que fue fundado en esta ciudad de Chiclayo el 18 de diciembre del año 1992 y desde entonces hasta hora estamos predicando el evangelio en este Ministerio y muchas almas se están convirtiendo al evangelio y miles de personas están siendo sanados, curados y libertados por el poder del Espíritu Santo y Dios nos está bendiciendo con muchos locales en diferentes lugares para predicar la palabra de Dios y el pueblo de Dios se reúna y también nos está bendiciendo con 9 emisoras que se utilizan exclusivamente para la obra de Dios como dice la palabra de Dios en el Evangelio de San Marcos cap. 16 del 15 al 18.</p>
						<p class="letra-modal">El Pastor Francisco Córdova Rodríguez se casó por revelación del Espíritu Santo con su esposa Carmen Palma Tafur en el año 1991 de los cuales Dios nos ha bendecido con siete hijos 5 varones y 2 mujeres cuyos nombres son Otoniel, Samuel, Timoteo, Daniel, Tabita, Pedro y Raquel, Córdova Palma. Esta relación es de mayor a menor.</p>
					</div>
				</div>
			</div>
			</div>
		</div>
		
		<div class="modal fade" id="modalResena" tabindex="-1" role="dialog" aria-labelledby="modalResenaTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<h5 class="modal-title" id="modalResenaTitle"> Reseña Histórica IPC</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>
				<div class="modal-body text-justify modal-body-scroll">
					<div>
						<p class="letra-modal">IGLESIA PENTECOSTAL LA COSECHA Fue fundado el 18 de Diciembre de 1992 por revelación del Espíritu Santo cuando el Pastor: Francisco Córdova Rodríguez, Presidente y Fundador de este Ministerio Cristiano, estaba en el país de Panamá, predicando la palabra de Dios.</p>
						<p class="letra-modal">Terminología del nombre de nuestra Iglesia</p>
						<img src="/web/assets/image/util/FRANCISCO-CORDOVA-RODRIGUEZ.jpg" hspace="5" vspace="5" class="imagen-pastor"/>
						
						<div class="row div-img">
							<div class="col-md-4">
								<b class="letra-modal">IGLESIA</b>
							</div>
							<div class="col-md-8">
								<span class="letra-modal">Personas que profesan el Evangelio (Hechos 14.27)</p>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<b class="letra-modal">PENTECOSTAL</b>
							</div>
							<div class="col-md-8">
								<p class="letra-modal">Poder de Dios (Hechos Cap. 2; y Cap. 10 Vers. 38)</p>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4">
								<b class="letra-modal">LA COSECHA</b>
							</div>
							<div class="col-md-8">
								<p class="letra-modal">Nombre profético que Dios desde tiempos antiguos por palabra del profeta Isaías dio a conocer. (Santa Biblia RVR 1960 Isaías 17.11)</p>
							</div>
						</div>

						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-8">
									<p class="letra-modal">El día que las plantes, las harás crecer, y harás que su simiente brote de mañana; pero la cosecha será arrebatada en el día de la angustia, y del dolor desesperado.</p>
							</div>
						</div>
						
						<p class="letra-modal">Predicamos el Evangelio del Señor Jesucristo, para salvación de las almas y con milagros y sanidades para los enfermos, conforme está escrito en la Santa Biblia en San Marcos capítulo 16 versículo 15 al 18 y creemos en la palabra de Dios desde Génesis hasta Apocalipsis sin agregar ni quitar a la palabra de Dios como dice en: </p>
						<p class="letra-modal"><b>DEUTERONOMIO CAP. 4 VERS. 2</b></p>
						<p class="letra-modal">No añadiréis a la palabra que yo os mando, ni disminuiréis de ella, para que guardéis los mandamientos de Jehová vuestro Dios que yo os ordeno.</p>
						<p class="letra-modal"><b>1 PEDRO CAP. 4 VERS. 11</b></p>
						<p class="letra-modal">Si alguno habla, hable conforme a las palabras de Dios; si alguno ministra, ministre conforme al poder que Dios da, para que en todo sea Dios glorificado por Jesucristo, a quien pertenecen la gloria y el imperio por los siglos de los siglos. Amén.</p>
						<p class="letra-modal"><b>APOCALISIS CAP. 22 VERS. 18 Y 19</b></p>
						<p class="letra-modal"><sup>18</sup> Yo testifico a todo aquel que oye las palabras de la profecía de este libro: Si alguno añadiere a estas cosas, Dios traerá sobre él las plagas que están escritas en este libro.</p>
						<p class="letra-modal"><sup>19</sup> Y si alguno quitare de las palabras del libro de esta profecía, Dios quitará su parte del libro de la vida, y de la santa ciudad y de las cosas que están escritas en este libro.</p>
						<p class="letra-modal">Damos gracias a Dios que desde que se abrieron las puertas de la IGLESIA PENTECOSTAL LA COSECHA hasta la actualidad, Dios se está manifestando grandemente con milagros, sanidades, bendiciones, perdonando al pecador y dando salvación para nuestras almas.</p>
						<p class="letra-modal">Tenemos cerca de 200 locales propios en diferentes lugares del Perú y un aproximado de 15 000 miembros activos de la IGLESIA PENTECOSTAL LA COSECHA a nivel nacional. También una gran sintonía a través de la emisora CADENA RADIO VISION y otras repetidoras. Asimismo muchos nos ven a través de la señal en vivo y en directo que se realiza a través de nuestra página web y el canal de YouTube.</p>
					</div>
				</div>
			</div>
			</div>
		</div>

		<!-- Departamentos -->
		<section id="sedes" class="padding ptb-xs-40 blog">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-lg-12 mt-sm-30 mt-xs-30 text-center"><h2 class="mb-30">Nuestras Sedes</h2></div>
					<div class="col-md-12 col-lg-6 mt-sm-30 mt-xs-30">
						<h2 class="mb-30">Departamentos</h2>
						<div id="accordion" class="mt-30 accord-tab" role="tablist" aria-multiselectable="true">
							@foreach ($elementosDepartamento as $index => $elementosDepartamento) 
							<div class="card">
								<div class="card-header card-departamento" role="tab" id="head-{{$elementosDepartamento->id}}">
									<h5 class="mb-0 panel-title departamentos">
										<a class="collapsed" data-toggle="collapse" data-parent="#accordion" 
										href="#collapse-{{$elementosDepartamento->id}}" 
										aria-expanded="false" aria-controls="collapse-{{$elementosDepartamento->id}}"> {{$elementosDepartamento->nombre}} <i class="fa fa-angle-up collape-up" aria-hidden="true"></i>
										</a>
									</h5>
								</div>

								<div id="collapse-{{$elementosDepartamento->id}}" class="bg-custom collapse" role="tabpanel" aria-labelledby="head-{{$elementosDepartamento->id}}" aria-expanded="false">
									<div class="card-block">
										<ul class="link blog-link card-link">
											@foreach ($elementosDepartamento->local as $elementosLocal) 
											<li class="cursor-pointer">
												<a onclick="verDatalleLocal({{$elementosLocal->id}})">
													<i class="fa fa-angle-double-right"></i>{{$elementosLocal->nombre}}
												</a>
											</li>
											@endforeach
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>

					</div>
					<div class="col-md-12 col-lg-6 blog-teble none" id="detalle_local">
						<div class="sec-title">
							<h2 class="mb-30">Detalle</h2>
						</div>
						<div class="nf-carousel-theme white mt-30">
							<div class="item">
								<div class="row blog-point">
									<div class="col-md-5">
										<div class="">
											<img id="imagen_local"/>
										</div>
									</div>
									<div class="col-md-7">
										<div class="right-blog-tet right-detalle-tet">
											<div class="post-entry">
												<div class="row">
													<div class="col-md-4">
														<span class="bold texto-local">LOCAL</span>
														<span class="right">:</span>
													</div>
													<div class="col-md-8">
														<span id="local">-</span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<span class="bold texto-local">DIRECCIÓN</span>
														<span class="right">:</span>
													</div>
													<div class="col-md-8">
														<span id="direccion">-</span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<span class="bold texto-local">CULTO</span>
														<span class="right">:</span>
													</div>
													<div class="col-md-8">
														<span id="dias">-</span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<span class="bold texto-local">HORARIOS</span>
														<span class="right">:</span>
													</div>
													<div class="col-md-8">
														<span id="horario">-</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Departamento-End -->

		<!-- Anuncios -->
		<section  id="anuncios" class="ptb-xs-40 blog">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-lg-12 blog-teble">
						<div class="sec-title">
							<h2 class="mb-30">Anuncios</h2>
						</div>
						<div class="owl-carousel anuncios-carousel nf-carousel-theme white mt-30">
							@foreach ($elementosAnuncio as $index => $elementosAnuncio)  
							<div class="item">
								<div class="row blog-point">
									<div class="col-md-5">
										<div class="blog-pic">
											{{ Html::image($elementosAnuncio->imagen, $elementosAnuncio->titulo, array()) }}
											<span class="event-calender blog-date"> {{$elementosAnuncio->dia}} <span>{{$elementosAnuncio->mes}}</span> </span>
										</div>
									</div>
									<div class="col-md-7">
										<div class="right-blog-tet">
											<div class="post-header mt-xs-30">
												<h5 class="cortar_texto_titulo">{{substr($elementosAnuncio->titulo, 0, 150)}}</h5>
											</div>
											<!--<div class="post-entry">
												<p class="cortar_texto_subtitulo">{!! substr(strip_tags($elementosAnuncio->descripcion), 0, 60) !!}...</p>
											</div>-->
											<div class="boton-view">
												<a class="link-modal" data-toggle="modal" onclick="verDatalleAnuncio({{$elementosAnuncio->id}})">Ver Mas</a>
											</div>
											<div class="icons-hover-black">
												@if($elementosAnuncio->ruta_facebook != null)
												<a href="{{$elementosAnuncio->ruta_facebook}}" target="_blanck" class="icon-redes facebook" title="Facebook"> <i class="fa fa-facebook"></i> </a>
												@endif
												@if($elementosAnuncio->ruta_twitter != null)
												<a href="{{$elementosAnuncio->ruta_twitter}}" target="_blanck" class="icon-redes twitter" title="Twitter"> <i class="fa fa-twitter"></i> </a>
												@endif
											</div>
										</div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Anuncios-End -->
			

		<!-- Modal -->
		<div class="modal fade" id="modalAnuncios" tabindex="-1" role="dialog" aria-labelledby="modalAnunciosTitulo" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<h5 class="modal-title" id="modalAnunciosTitulo">-</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>
				<div class="modal-body text-justify modal-body-scroll">
					<div>
						<img id="modalAnunciosImagen" src="" hspace="5" vspace="5" class="imagen-pastor"/>
						<p class="letra-modal" id="modalAnunciosDescripcion"></p>
						<div class="icons-hover-black">
							<a id="modalAnunciosFacebook" href="" target="_blanck" class="icon-redes facebook none" title="Facebook"> <i class="fa fa-facebook"></i> </a>
							<a id="modalAnunciosTwitter" href="" target="_blanck" class="icon-redes twitter none" title="Twitter"> <i class="fa fa-twitter"></i> </a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>

		<!-- Noticas -->
		<section  id="noticas" class="ptb-xs-40 blog">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-lg-12 blog-teble">
						<div class="sec-title">
							<h2 class="mb-30">Noticias</h2>
						</div>
						<div class="owl-carousel noticias-carousel nf-carousel-theme white mt-30">
							@foreach ($elementosNoticia as $index => $elementosNoticia)  
							<div class="item">
								<div class="row blog-point">
									<div class="col-md-5">
										<div class="blog-pic">
											{{ Html::image($elementosNoticia->imagen, $elementosNoticia->titulo, array()) }}
											<span class="event-calender blog-date"> {{$elementosNoticia->dia}} <span>{{$elementosNoticia->mes}}</span> </span>
										</div>
									</div>
									<div class="col-md-7">
										<div class="right-blog-tet">
											<div class="post-header mt-xs-30">
												<h5 class="cortar_texto_titulo">{{substr($elementosNoticia->titulo, 0, 150)}}</h5>
											</div>
											<!--<div class="post-entry">
												<p class="cortar_texto_subtitulo">{!! substr(strip_tags($elementosNoticia->descripcion), 0, 60) !!}...</p>
											</div>-->
											<div class="boton-view">
												<a class="link-modal" data-toggle="modal" onclick="verDatalleNoticia({{$elementosNoticia->id}})">Ver Mas</a>
											</div>
											<div class="icons-hover-black">
												@if($elementosNoticia->ruta_facebook != null)
												<a href="{{$elementosNoticia->ruta_facebook}}" target="_blanck" class="icon-redes facebook" title="Facebook"> <i class="fa fa-facebook"></i> </a>
												@endif
												@if($elementosNoticia->ruta_twitter != null)
												<a href="{{$elementosNoticia->ruta_twitter}}" target="_blanck" class="icon-redes twitter" title="Twitter"> <i class="fa fa-twitter"></i> </a>
												@endif
											</div>
										</div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Noticas-End -->

		<!-- Modal -->
		<div class="modal fade" id="modalNoticias" tabindex="-1" role="dialog" aria-labelledby="modalNoticiasTitulo" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<h5 class="modal-title" id="modalNoticiasTitulo">-</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>
				<div class="modal-body text-justify modal-body-scroll">
					<div>
						<img id="modalNoticiasImagen" src="" hspace="5" vspace="5" class="imagen-pastor"/>
						<p id="modalNoticiasDescripcion" class="letra-modal">-</p>
						<div class="icons-hover-black">
								<a id="modalNoticiasFacebook" href="" target="_blanck" class="icon-redes facebook none" title="Facebook"> <i class="fa fa-facebook"></i> </a>
								<a id="modalNoticiasTwitter" href="" target="_blanck" class="icon-redes twitter none" title="Twitter"> <i class="fa fa-twitter"></i> </a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>

		<!-- Testimonios -->
		<section id="testimonio" class="ptb-xs-40 blog">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-lg-12 blog-teble">
						<div class="sec-title">
							<h2 class="mb-30">Testimonios</h2>
						</div>
						<div class="owl-carousel testimonio-carousel nf-carousel-theme white mt-30">
							@foreach ($elementosTestimonio as $index => $elementosTestimonio)  
							<div class="item">
								<div class="row blog-point">
									<div class="col-md-5">
										<div class="blog-pic">
											{{ Html::image($elementosTestimonio->imagen, $elementosTestimonio->titulo, array()) }}
											<span class="event-calender blog-date"> {{$elementosTestimonio->dia}} <span>{{$elementosTestimonio->mes}}</span> </span>
										</div>
									</div>
									<div class="col-md-7">
										<div class="right-blog-tet">    
											<div class="post-header mt-xs-30">
												<h5 class="cortar_texto_titulo">{{substr($elementosTestimonio->titulo, 0, 150)}}</h5>
											</div>
											<!--<div class="post-entry">
												<p class="cortar_texto_subtitulo">{!! substr(strip_tags($elementosTestimonio->descripcion), 0, 60) !!}...</p>
											</div>-->
											<div class="boton-view">
												<a class="link-modal" data-toggle="modal" onclick="verDatalleTestimoios({{$elementosTestimonio->id}})">Ver Mas</a>
											</div>
											<div class="icons-hover-black">
												@if($elementosTestimonio->ruta_facebook != null)
												<a href="{{$elementosTestimonio->ruta_facebook}}" target="_blanck" class="icon-redes facebook" title="Facebook"> <i class="fa fa-facebook"></i> </a>
												@endif
												@if($elementosTestimonio->ruta_twitter != null)
												<a href="{{$elementosTestimonio->ruta_twitter}}" target="_blanck" class="icon-redes twitter" title="Twitter"> <i class="fa fa-twitter"></i> </a>
												@endif
											</div>
										</div>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Noticas-End -->

		<!-- Modal -->
		<div class="modal fade" id="modalTestimonio" tabindex="-1" role="dialog" aria-labelledby="modalTestimonioTitulo" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<h5 class="modal-title" id="modalTestimonioTitulo">-</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>
				<div class="modal-body text-justify modal-body-scroll">
					<div>
						<img id="modalTestimonioImagen" src="" hspace="5" vspace="5" class="imagen-pastor"/>
						<p id="modalTestimonioDescripcion" class="letra-modal">-</p>
						<div class="icons-hover-black">
								<a id="modalTestimonioFacebook" href="" target="_blanck" class="icon-redes facebook none" title="Facebook"> <i class="fa fa-facebook"></i> </a>
								<a id="modalTestimonioTwitter" href="" target="_blanck" class="icon-redes twitter none" title="Twitter"> <i class="fa fa-twitter"></i> </a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>

		<div class="modal fade" id="modalMusica" tabindex="-1" role="dialog" 
			aria-labelledby="modalMusicaTitulo" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<h5 class="modal-title" id="modalMusicaTitulo">Música</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>
				<div class="modal-body text-justify modal-body-scroll">
					<div class="resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
						<ul class="music-list" data-player-id="player" id="ul_musica">

						</ul>
					</div>
				</div>
				<div class="modal-footer" id="modal-footer">
					<div class="col-md-3 col-lg-3 d-flex align-items-center">
						<button class="bnt btn-text sent-but mt-xs-30 font-12" type="button" 
						name="button" onclick="verDatalleCarpetaMusica(1)">
							Regresar
						</button>
					</div>
				</div>
			</div>
			</div>
		</div>

		<div class="modal fade" id="modalGaleria" tabindex="-1" role="dialog" 
			aria-labelledby="modalGaleriaTitulo" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<h5 class="modal-title" id="modalGaleriaTitulo">Galería</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				</div>
				<div class="modal-body text-justify modal-body-scroll" id="modal_body_galeria">
					<div class="resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
						<ul class="music-list" data-player-id="player" id="ul_galeria">
						</ul>
						<section id="galerias-imagenes">
							<div class="gal-container row" id="container_galeria_imagen">
								<!--
								<div class="col-md-4 col-sm-4 co-xs-12 gal-item">
									<div class="box">
										<a href="#" data-toggle="modal" data-target="#1">
										<img src="http://localhost:8002/images/local/i.p.c._sede_nacional_-_chiclayo.png">
										</a>
										<div class="modal fade modal-dialog-galeria" id="1" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<div class="modal-content modal-content-transparent" onclick="cerrarModalGaleria()">
													<button type="button" class="close"><span aria-hidden="true">×</span></button>
												<div class="modal-body">
													<img class="imagen-modal" src="http://localhost:8002/images/local/i.p.c._sede_nacional_-_chiclayo.png">
												</div>
													<div class="col-md-12 description">
														<p>This is the first one on my Gallery</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							-->									
							</div>
						</section>
					</div>
				</div>
				<div class="modal-footer" id="modal-footer-galeria">
					<div class="col-md-3 col-lg-3 d-flex align-items-center">
						<button class="bnt btn-text sent-but mt-xs-30 font-12" type="button" 
						name="button" onclick="verDatalleCarpetaGaleria(1)">
							Regresar
						</button>
					</div>
				</div>
			</div>
			</div>
		</div>

		<!-- feadback -->
		<div id="contactenos" class="page-header header-oracion">
			<div class="container">
				<h1 class="title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Contáctenos</font></font></h1>
			</div>
			<div class="breadcrumb-box">
				<div class="container">
					<nav aria-label="breadcrumb" role="navigation">
						<ol class="breadcrumb">
							<li class="breadcrumb-item">
								<a id="pedidos" class="color-contactanos cursor-pointer" onclick="contactanos(0)">
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">
											Pedidos de Oración
										</font>
									</font>
								</a>
							</li>
							<li class="breadcrumb-item active" aria-current="page">
								<a id="escribenos"  onclick="contactanos(1)" class="cursor-pointer">
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">
											Escríbenos
										</font>
									</font>
								</a>
							</li>
							<li class="breadcrumb-item active" aria-current="page">
								<a id="testimonios"  onclick="contactanos(2)" class="cursor-pointer">
									<font style="vertical-align: inherit;">
										<font style="vertical-align: inherit;">
											Cuéntanos tu Testimonio
										</font>
									</font>
								</a>
							</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
		<section id="feadback" class="spadding ptb-xs-40 bg-gray" style="padding: 50px 0 50px 0;">
			<div class="container" id="section-oraciones">
				<div class="row mb-30 mb-xs-30">
					<div class="col-md-12">
						<h2>Pedidos de Oración</h2>
					</div>
				</div>
				<div class="row mb-30 mb-xs-30">
					<div class="col-md-12">
						<span>
							<p>Las Oraciones serán transmitidas por Cadena Radio Visión E.I.R.L., el mismo dia de sus pedidos de oración, minutos antes de las 8:30 pm. </p>
							<p>Si usted quiere ayudar a esta obre de Fé puede enviar sus ofrendas a Calle Juan Fanning 457 Chiclayo - Perú.</p>
							<p class="bold">La promesa esta en Malaquias 3:18</p>
						</span>
					</div>
				</div>

				<div class="row">

					<div class=" col-md-12 col-lg-12">

						<!-- Contact FORM -->
						<form class="contact-form " id="contact">
							
							<!-- END IF MAIL SENT SUCCESSFULLY -->
							<div class="row">
								<div class="col-md-4 col-lg-4">
									<div class="form-field">
										<input class="input-sm form-full" id="nombres_po" type="text" name="form-name" placeholder="Nombre Completo">
									</div>
								</div>

								<div class="col-md-4 col-lg-4">
									<div class="form-field">
										<input class="input-sm form-full" id="correo_po" type="text" name="form-email" placeholder="Dirección Email">
									</div>
								</div>

								<div class="col-md-4 col-lg-4">
									<div class="form-field">
										<input class="input-sm form-full" id="motivo_po" type="text" name="form-subject" placeholder="Motivo Oracion">
									</div>
								</div>

								<div class="col-md-4 col-lg-4">
									<div class="form-field">
										<select class="input-sm form-full" id="pais_po">
											<option value="Antigua y Barbuda">Antigua y Barbuda</option>
											<option value="Argentina">Argentina</option>
											<option value="Bahamas">Bahamas</option>
											<option value="Barbados">Barbados</option>
											<option value="Belice">Belice</option>
											<option value="Bolivia">Bolivia</option>
											<option value="Brasil">Brasil</option>
											<option value="Canadá">Canadá</option>
											<option value="Chile">Chile</option>
											<option value="Colombia">Colombia</option>
											<option value="Costa Rica">Costa Rica</option>
											<option value="Cuba">Cuba</option>
											<option value="Dominica">Dominica</option>
											<option value="Ecuador">Ecuador</option>
											<option value="El Salvador">El Salvador</option>
											<option value="España">España</option>
											<option value="Estados Unidos">Estados Unidos</option>
											<option value="Granada">Granada</option>
											<option value="Guatemala">Guatemala</option>
											<option value="Guyana">Guyana</option>
											<option value="Haití­">Haití­</option>
											<option value="Honduras">Honduras</option>
											<option value="Italia">Italia</option>
											<option value="Jamaica">Jamaica</option>
											<option value="México">México</option>
											<option value="Nicaragua">Nicaragua</option>
											<option value="Panamá">Panamá</option>
											<option value="Paraguay">Paraguay</option>
											<option value="Perú">Perú</option>
											<option value="República Dominicana">República Dominicana</option>
											<option value="San Cristóbal y Nieves">San Cristóbal y Nieves</option>
											<option value="San Vicente y las Granadinas">San Vicente y las Granadinas</option>
											<option value="Santa Lucí­a">Santa Lucí­a</option>
											<option value="Surinam">Surinam</option>
											<option value="Trinidad y Tobago">Trinidad y Tobago</option>
											<option value="Uruguay">Uruguay</option>
											<option value="Venezuela">Venezuela</option>
										</select>
									</div>
								</div>

								<div class="col-md-8 col-lg-8">
									<div class="form-field">
										<textarea class="form-full" id="descripcion_po" rows="7" name="form-message" placeholder="Solicito Oracion por"></textarea>
									</div>
								</div>

								<div class="col-md-2 col-lg-2 d-flex align-items-center">
									<button class="bnt btn-text sent-but mt-xs-30" 
									type="button" name="button" onclick="correoPedidoOracion()">
										Enviar
									</button>

									<!--
									<button
										class="g-recaptcha"
										data-sitekey="6Lf0MUoUAAAAAGKGmXX11eRlg1ePRFPNsBQFBmZD"
										data-callback="YourOnSubmitFn">
										Submit
									</button>
									-->
									
								</div>
							</div>
						</form>
						<!-- END Contact FORM -->
					</div>

				</div>

			</div>

			<div class="container none" id="section-contacto">
				<div class="row mb-30 mb-xs-30">
					<div class="col-md-12">
						<h2>Escríbenos</h2>
					</div>
				</div>

				<div class="row">
					<div class=" col-md-12 col-lg-12">
						<!-- Contact FORM -->
						<form class="contact-form " id="contact">
							<!-- END IF MAIL SENT SUCCESSFULLY -->
							<div class="row">
								<div class="col-md-4 col-lg-4">
									<div class="form-field">
										<input class="input-sm form-full" id="nombres_es" type="text" name="form-name" placeholder="Nombre Completo">
									</div>
								</div>

								<div class="col-md-4 col-lg-4">
									<div class="form-field">
										<input class="input-sm form-full" id="correo_es" type="text" name="form-email" placeholder="Dirección Email">
									</div>
								</div>

								<div class="col-md-4 col-lg-4">
									<div class="form-field">
										<input class="input-sm form-full" id="telefono_es" type="text" name="form-subject" placeholder="Número de Teléfono">
									</div>
								</div>

								<div class="col-md-10 col-lg-10">
									<div class="form-field">
										<textarea class="form-full" id="descripcion_es" rows="7" name="form-message" placeholder="Ingrese su mensaje"></textarea>
									</div>
								</div>
								<div class="col-md-2 col-lg-2 d-flex align-items-center">
									<button class="bnt btn-text sent-but mt-xs-30" type="button" onclick="correoEscribenos()">
										Enviar
									</button>
								</div>
							</div>
						</form>
						<!-- END Contact FORM -->
					</div>

				</div>

			</div>

			<div class="container none" id="section-testimonio">
				<div class="row mb-30 mb-xs-30">
					<div class="col-md-12">
						<h2>Cuéntanos tu Testimonio</h2>
					</div>
				</div>

				<div class="row">
					<div class=" col-md-12 col-lg-12">
						<!-- Contact FORM -->
						<form class="contact-form " id="contact">
							<!-- END IF MAIL SENT SUCCESSFULLY -->
							<div class="row">
								<div class="col-md-4 col-lg-4">
									<div class="form-field">
										<input class="input-sm form-full" id="nombres_te" type="text" name="form-name" placeholder="Nombre Completo">
									</div>
								</div>

								<div class="col-md-4 col-lg-4">
									<div class="form-field">
										<input class="input-sm form-full" id="correo_te" type="text" name="form-email" placeholder="Dirección Email">
									</div>
								</div>

								<div class="col-md-4 col-lg-4">
									<div class="form-field">
										<input class="input-sm form-full" id="telefono_te" type="text" name="form-subject" placeholder="Número de Teléfono">
									</div>
								</div>

								<div class="col-md-10 col-lg-10">
									<div class="form-field">
										<textarea class="form-full" id="descripcion_te" rows="7" name="form-message" placeholder="Cuéntanos tus Testimonios"></textarea>
									</div>
								</div>
								<div class="col-md-2 col-lg-2 d-flex align-items-center">
									<button class="bnt btn-text sent-but mt-xs-30" type="button" onclick="correoTestimonio()">
										Enviar
									</button>
								</div>
							</div>
						</form>
						<!-- END Contact FORM -->
					</div>

				</div>

			</div>
		</section>

		<!-- Footer -->
		<footer class="footer pt-80 pt-xs-60">
			<div class="container">
				<!--Footer Info -->
				<div class="row footer-info mb-60">
					<div class="col-lg-4 col-md-4 col-xs-12 mb-sm-30">
						<h4 class="mb-30">Contáctenos</h4>
						<address>
							<i class="fa fa-map-marker"></i>  Calle Juan Fanning 457 - CHICLAYO PERÚ
						</address>
						<ul class="link-small">
							<li>
								<a href="mailto:info@iplacosecha.pe"><i class="fa fa-envelope-o"></i>  info@iplacosecha.pe</a>
							</li>
							<li>
								<a><i class="fa fa-phone"></i>  074 225814</a>
							</li>
						</ul>
						<div class="icons-hover-black">
							<a href="javascript:avoid(0);" title="Facebook"> <i class="fa fa-facebook facebook-footer"></i> </a>
							<a href="javascript:avoid(0);" title="Twitter"> <i class="fa fa-twitter twitter-footer"></i> </a>
							<a href="javascript:avoid(0);" title="Youtube"> <i class="fa fa-youtube youtube-footer"></i> </a>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-xs-12 mb-sm-30">
						<h4 class="mb-30">Enlaces</h4>
						<ul class="link blog-link">
							<li>
								<a href="#inicio" class="link_scroll_inicio"><i class="fa fa-angle-double-right"></i> Inicio</a>
							</li>
							<li>
								<a href="#biografia" class="link_scroll_biografia"><i class="fa fa-angle-double-right"></i> Biografía</a>
							</li>
							<li>
								<a href="#sedes" class="link_scroll_sedes"><i class="fa fa-angle-double-right"></i> Sedes Ipc</a>
							</li>
							<li>
								<a href="#anuncios" class="link_scroll_anuncios"><i class="fa fa-angle-double-right"></i> Campañas</a>
							</li>
							<li>
								<a href="#contactenos" class="link_scroll_contactenos"><i class="fa fa-angle-double-right"></i> Contáctenos</a>
							</li>
						</ul>
					</div>
					<div class="col-lg-5 col-md-5 col-xs-12 mb-sm-30">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.0355868593083!2d-79.83653648568321!3d-6.765515495109548!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x904ceed3f6ef9e79%3A0x77a10f42f6c6890!2sJuan+Fanning+457%2C+Chiclayo+14001!5e0!3m2!1ses!2spe!4v1519960047352" 
						height="300" frameborder="0" style="border:0; width: 100%;" allowfullscreen></iframe>
					</div>
				</div>
				<!-- End Footer Info -->
			</div>
			<!-- Copyright Bar -->
			<div class="copyright">
				<div class="container">
					<p class="">
						© 2018 <a href="http://www.cvsoluciones.com/inicio/" target="_blanck"><b>CV Soluciones</b></a>. All Rights Reserved.
					</p>
				</div>
			</div>
			<!-- End Copyright Bar -->
		</footer>
		<!-- Footer -->
		<a id="back-to-top" href="#" class="back-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i> </a>

		
		<script type="text/javascript" src="/web/assets/js/tether.min.js"></script>
		<script type="text/javascript" src="/web/assets/js/bootstrap.min.js"></script>
		<!-- carousel Js -->

		<script src="/web/assets/js/owl.carousel.js" type="text/javascript"></script>
		<!-- imagesloaded Js -->
		<script src="/web/assets/js/imagesloaded.pkgd.min.js" type="text/javascript"></script>
		<!-- masonry,isotope Effect Js -->
		<script src="/web/assets/js/isotope.pkgd.min.js" type="text/javascript"></script>
		<script src="/web/assets/js/masonry.pkgd.min.js" type="text/javascript"></script>
		<script src="/web/assets/js/jquery.appear.js" type="text/javascript"></script>
		<!-- parallax Js -->
		<script src="/web/assets/js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
		<script src="/web/assets/js/jquery.appear.js" type="text/javascript"></script>
		<!-- revolution Js -->
		<script type="text/javascript" src="/web/assets/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="/web/assets/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="/web/assets/js/revolution.extension.slideanims.min.js"></script>
		<script type="text/javascript" src="/web/assets/js/revolution.extension.layeranimation.min.js"></script>
		<script type="text/javascript" src="/web/assets/js/revolution.extension.navigation.min.js"></script>
		<script type="text/javascript" src="/web/assets/js/revolution.extension.parallax.min.js"></script>
		<script type="text/javascript" src="/web/assets/js/jquery.revolution.js"></script>
		<!-- custom Js -->
		<script type="text/javascript" src="/web/assets/js/custom.js"></script>

		<script>
			$('.link_scroll_inicio').on('click', function(e) {
				e.preventDefault();
				$("html, body").animate({scrollTop: $('#inicio').offset().top }, 1000);
			});

			$('.link_scroll_biografia').on('click', function(e) {
				e.preventDefault();
				$("html, body").animate({scrollTop: $('#biografia').offset().top }, 1000);
			});

			$('.link_scroll_sedes').on('click', function(e) {
				e.preventDefault();
				$("html, body").animate({scrollTop: $('#sedes').offset().top }, 1000);
			});

			$('.link_scroll_anuncios').on('click', function(e) {
				e.preventDefault();
				$("html, body").animate({scrollTop: $('#anuncios').offset().top }, 1000);
			});

			$('.link_scroll_contactenos').on('click', function(e) {
				e.preventDefault();
				$("html, body").animate({scrollTop: $('#contactenos').offset().top }, 1000);
			});
			
			function contactanos(bandera){
				if(bandera == 0){
					$("#section-oraciones").removeClass("none");
					$("#section-contacto").addClass("none");
					$("#section-testimonio").addClass("none");
					$("#pedidos").addClass("color-contactanos");
					$("#escribenos").removeClass("color-contactanos");
					$("#testimonios").removeClass("color-contactanos");
				}else if(bandera == 1){
					$("#section-oraciones").addClass("none");
					$("#section-contacto").removeClass("none");
					$("#section-testimonio").addClass("none");
					$("#pedidos").removeClass("color-contactanos");
					$("#escribenos").addClass("color-contactanos");
					$("#testimonios").removeClass("color-contactanos");
				}else{
					$("#section-oraciones").addClass("none");
					$("#section-contacto").addClass("none");
					$("#section-testimonio").removeClass("none");
					$("#pedidos").removeClass("color-contactanos");
					$("#escribenos").removeClass("color-contactanos");
					$("#testimonios").addClass("color-contactanos");
				}
			}

			function biografia(bandera){
				if(bandera == 0){
					$("#div_fundador").removeClass("none");
					$("#div_resena").addClass("none");
					$("#fundador").addClass("color-contactanos");
					$("#resena").removeClass("color-contactanos");
				}else{
					$("#div_fundador").addClass("none");
					$("#div_resena").removeClass("none");
					$("#fundador").removeClass("color-contactanos");
					$("#resena").addClass("color-contactanos");
				}
			}

			function verDatalleLocal(id){
				$("#preloader").css("background", "transparent");
				$("#preloader").css("display", "block");

				var ruta = "/principal/ruta_mostrar_detalle_local";
				var token = $('#token_mostrar_detalle_local').val();
				var element = {id:id};
				$.ajax({
					url: ruta,
					headers: {'X-CSRF-TOKEN':token},
					type:"POST",
					dataType: 'json',
					data: element,
					success: function(data){          
						var elemento = data.elementos;
						$("#imagen_local").attr("src",elemento["imagen"]);
						$("#imagen_local").attr("alt",elemento["nombre"]);
						$("#local").html(elemento["nombre"]);
						$("#direccion").html(elemento["direccion"]);
						$("#dias").html(elemento["dias_culto"]);
						$("#horario").html(elemento["horario_culto"]);
						$("#detalle_local").removeClass("none");
						$("#preloader").css("display", "none");
						$("html, body").animate({scrollTop: $('#detalle_local').offset().top }, 1000);
					},
					error: function(error) {
						console.log(error);
					}             
				});
			}

			function verDatalleNoticia(id){
				$("#preloader").css("background", "transparent");
				$("#preloader").css("display", "block");

				var ruta = "/principal/ruta_mostrar_detalle_noticia";
				var token = $('#token_mostrar_detalle_noticia').val();
				var element = {id:id};
				$.ajax({
					url: ruta,
					headers: {'X-CSRF-TOKEN':token},
					type:"POST",
					dataType: 'json',
					data: element,
					success: function(data){          
						var elemento = data.elementos;
						$('#modalNoticias').modal('show');
						$("#modalNoticiasImagen").attr("src",elemento["imagen"]);
						$("#modalNoticiasImagen").attr("alt",elemento["titulo"]);
						$("#modalNoticiasTitulo").html(elemento["titulo"]);
						$("#modalNoticiasDescripcion").html(elemento["descripcion"]);
						$("#preloader").css("display", "none");

						if(elemento["ruta_facebook"] != null){
							$("#modalNoticiasFacebook").removeClass("none");
							$("#modalNoticiasFacebook").attr("href",elemento["ruta_facebook"]);
						}

						if(elemento["ruta_twitter"] != null){
							$("#modalNoticiasTwitter").removeClass("none");
							$("#modalNoticiasTwitter").attr("href",elemento["ruta_twitter"]);
						}
					},
					error: function(error) {
						console.log(error);
					}             
				});
			}

			function verDatalleTestimoios(id){
				$("#preloader").css("background", "transparent");
				$("#preloader").css("display", "block");

				var ruta = "/principal/ruta_mostrar_detalle_testimonio";
				var token = $('#token_mostrar_detalle_testimonio').val();
				var element = {id:id};
				$.ajax({
					url: ruta,
					headers: {'X-CSRF-TOKEN':token},
					type:"POST",
					dataType: 'json',
					data: element,
					success: function(data){          
						var elemento = data.elementos;
						$('#modalTestimonio').modal('show');
						$("#modalTestimonioImagen").attr("src",elemento["imagen"]);
						$("#modalTestimonioImagen").attr("alt",elemento["titulo"]);
						$("#modalTestimonioTitulo").html(elemento["titulo"]);
						$("#modalTestimonioDescripcion").html(elemento["descripcion"]);
						$("#preloader").css("display", "none");

						if(elemento["ruta_facebook"] != null){
							$("#modalTestimonioFacebook").removeClass("none");
							$("#modalTestimonioFacebook").attr("href",elemento["ruta_facebook"]);
						}

						if(elemento["ruta_twitter"] != null){
							$("#modalTestimonioTwitter").removeClass("none");
							$("#modalTestimonioTwitter").attr("href",elemento["ruta_twitter"]);
						}
					},
					error: function(error) {
						console.log(error);
					}             
				});
			}

			function verDatalleAnuncio(id){
				$("#preloader").css("background", "transparent");
				$("#preloader").css("display", "block");

				var ruta = "/principal/ruta_mostrar_detalle_anuncio";
				var token = $('#token_mostrar_detalle_anuncio').val();
				var element = {id:id};
				$.ajax({
					url: ruta,
					headers: {'X-CSRF-TOKEN':token},
					type:"POST",
					dataType: 'json',
					data: element,
					success: function(data){ 
						var elemento = data.elementos;
						$('#modalAnuncios').modal('show');
						$("#modalAnunciosImagen").attr("src",elemento["imagen"]);
						$("#modalAnunciosImagen").attr("alt",elemento["titulo"]);
						$("#modalAnunciosTitulo").html(elemento["titulo"]);
						$("#modalAnunciosDescripcion").html(elemento["descripcion"]);
						$("#preloader").css("display", "none");
						
						if(elemento["ruta_facebook"] != null){
							$("#modalAnunciosFacebook").removeClass("none");
							$("#modalAnunciosFacebook").attr("href",elemento["ruta_facebook"]);
						}

						if(elemento["ruta_twitter"] != null){
							$("#modalAnunciosTwitter").removeClass("none");
							$("#modalAnunciosTwitter").attr("href",elemento["ruta_twitter"]);
						}
					},
					error: function(error) {
						console.log(error);
					}             
				});
			}
			
			function verDatalleCarpetaMusica(estado){
				$("#preloader").css("background", "transparent");
				$("#preloader").css("display", "block");

				var ruta = "/principal/ruta_mostrar_detalle_carpeta_musica";
				var token = $('#token_mostrar_detalle_carpeta_musica').val();

				var urlMusica = "http://iplacosecha.pe/audiosipc/";
				var element = {url:urlMusica};
				$.ajax({
					url: ruta,
					headers: {'X-CSRF-TOKEN':token},
					type:"POST",
					dataType: 'json',
					data: element,
					success: function(data){ 
						$("#modal-footer").addClass("none");
						$("#ul_musica").html("");
						var elemento = data.elementos;
						
						if(estado == 0) $('#modalMusica').modal('show');
					
						for(var i=0; i<data.cantidad; i++){
							var nuevaRuta = urlMusica+elemento[i];
							var html = 
							'<li onclick="verDatalleMusica(\''+nuevaRuta+'\')" class="">'+
								'<a>'+
									'<i class="fa fa-play-circle-o" aria-hidden="true"></i>'+
									'<h5 class="title">'+elemento[i]+'</h5>'+
									//'<span class="small offset-4 text-contrast-variant-1">26,769 downloads</span>'+
								'</a>'+
							'</li>';
							$("#ul_musica").append(html);
						}

						$("#preloader").css("display", "none");
					},
					error: function(error) {
						alert(error);
					}             
				});
			}
			
			function verDatalleMusica(urlMusica){
				$("#preloader").css("background", "transparent");
				$("#preloader").css("display", "block");
				
				var ruta = "/principal/ruta_mostrar_detalle_musica";
				var token = $('#token_mostrar_detalle_musica').val();
				//var urlMusica2 = "http://www.cvsoluciones.com/archivosIPC/MUSICA/MUSICA%20ECUATORIANA/ALBAZOS%20ECUATORIANO/";
				var element = {url: urlMusica};
				$.ajax({
					url: ruta,
					headers: {'X-CSRF-TOKEN':token},
					type:"POST",
					dataType: 'json',
					data: element,
					success: function(data){       
						$("#modal-footer").removeClass("none");   
						$("#ul_musica").html("");
						var elemento = data.elementos;
						//$('#modalMusica').modal('show');
						
						for(var i=0; i<data.cantidad; i++){
							var html = 
							'<li class="">'+
								'<a href="'+urlMusica+elemento[i]+'" target="_blanck">'+
									'<i class="fa fa-play-circle-o" aria-hidden="true"></i>'+
									'<h5 class="title">'+elemento[i]+'</h5>'+
									//'<span class="small offset-4 text-contrast-variant-1">26,769 downloads</span>'+
								'</a>'+
							'</li>';
							$("#ul_musica").append(html);
						}


						$("#preloader").css("display", "none");
					},
					error: function(error) {
						alert(error);
					}             
				});
			}

			function verDatalleCarpetaGaleria(estado){
				$("#preloader").css("background", "transparent");
				$("#preloader").css("display", "block");

				var ruta = "/principal/ruta_mostrar_detalle_carpeta_galeria";
				var token = $('#token_mostrar_detalle_carpeta_galeria').val();

				var element = {};
				$.ajax({
					url: ruta,
					headers: {'X-CSRF-TOKEN':token},
					type:"POST",
					dataType: 'json',
					data: element,
					success: function(data){ 
						$("#modal-footer-galeria").addClass("none");
						$("#galerias-imagenes").addClass("none");
						$("#modal_body_galeria").removeClass("modal-body-scroll-height");

						$("#ul_galeria").html("");
						var elemento = data.elementos;
						
						if(estado == 0) $('#modalGaleria').modal('show');

						for(var i=0; i<data.cantidad; i++){
							var html = 
							'<li onclick="verDatalleGaleria('+elemento[i].id+')" class="">'+
								'<a>'+
									'<i class="fa fa-camera-retro" aria-hidden="true"></i>'+
									'<h5 class="title">'+elemento[i].nombre+'</h5>'+
								'</a>'+
							'</li>';
							$("#ul_galeria").append(html);
						}
						$("#preloader").css("display", "none");
					},
					error: function(error) {
						alert(error);
					}             
				});
			}

			function verDatalleGaleria(categoria_galeria_id){
				$("#preloader").css("background", "transparent");
				$("#preloader").css("display", "block");

				var ruta = "/principal/ruta_mostrar_detalle_galeria";
				var token = $('#token_mostrar_detalle_galeria').val();

				var element = {categoria_galeria_id:categoria_galeria_id};
				$.ajax({
					url: ruta,
					headers: {'X-CSRF-TOKEN':token},
					type:"POST",
					dataType: 'json',
					data: element,
					success: function(data){ 
						$("#modal-footer-galeria").removeClass("none");
						$("#galerias-imagenes").removeClass("none");
						$("#modal_body_galeria").addClass("modal-body-scroll-height");
						$("#ul_galeria").html("");
						
						$("#container_galeria_imagen").html("");
						var elemento = data.elementos;

						for(var i=0; i<data.cantidad; i++){
							var html = 
							'<div class="col-md-4 col-sm-4 co-xs-12 gal-item">'+
								'<div class="box">'+
									'<a href="#" data-toggle="modal" data-target="#'+(i+1)+'" title="'+elemento[i].titulo+'">'+
										'<img src="'+elemento[i].imagen+'">'+
									'</a>'+
									'<div class="modal fade modal-dialog-galeria" id="'+(i+1)+'" tabindex="-1" role="dialog">'+
										'<div class="modal-dialog" role="document">'+
											'<div class="modal-content modal-content-transparent" onclick="cerrarModalGaleria('+(i+1)+')">'+
												'<button type="button" class="close"><span aria-hidden="true">×</span></button>'+
											'<div class="modal-body">'+
												'<img class="imagen-modal" src="'+elemento[i].imagen+'">'+
											'</div>'+
												'<div class="col-md-12 description">'+
													'<p>'+elemento[i].titulo+'</p>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>';
							$("#container_galeria_imagen").append(html);
						}
						$("#preloader").css("display", "none");
					},
					error: function(error) {
						alert(error);
					}             
				});
			}

			function cerrarModalGaleria(i){
				$('#'+i).modal('hide');
			}

			function correoPedidoOracion(){
				if($("#nombres_po").val().trim().length == 0 ||
				$("#correo_po").val().trim().length == 0 ||
				$("#motivo_po").val().trim().length == 0 ||
				$("#descripcion_po").val().trim().length == 0
				){
					$.notify({message: 'Completar los datos correspondientes'},{type: 'danger'});
				}else{
					registrarCorreoPedidoOracion();
				}
			}
			function registrarCorreoPedidoOracion(){
				$("#preloader").css("background", "transparent");
				$("#preloader").css("display", "block");

				var ruta = "/principal/ruta_correo_pedido_oracion";
				var token = $('#token_correo_pedido_oracion').val();
				var element = {
					nombres: $("#nombres_po").val(),
					correo: $("#correo_po").val(),
					motivo: $("#motivo_po").val(),
					pais: $("#pais_po").val(),
					descripcion: $("#descripcion_po").val()
				};
				$.ajax({
					url: ruta,
					headers: {'X-CSRF-TOKEN':token},
					type:"POST",
					dataType: 'json',
					data: element,
					success: function(data){ 
						$.notify({message: 'Se envió corretamente el pedido de oración'},{type: 'success'});
						$("#nombres_po").val("");
						$("#correo_po").val("");
						$("#motivo_po").val("");
						$("#descripcion_po").val("");
						$("#preloader").css("display", "none");
						$(".link_scroll_inicio").click();
					},
					error: function(error) {
						console.log(error);
					}             
				});
			}

			function correoEscribenos(){
				if($("#nombres_es").val().trim().length == 0 ||
				$("#correo_es").val().trim().length == 0 ||
				$("#telefono_es").val().trim().length == 0 ||
				$("#descripcion_es").val().trim().length == 0
				){
					$.notify({message: 'Completar los datos correspondientes'},{type: 'danger'});
				}else{
					registrarCorreoEscribenos();
				}
			}
			function registrarCorreoEscribenos(){
				$("#preloader").css("background", "transparent");
				$("#preloader").css("display", "block");

				var ruta = "/principal/ruta_correo_escribenos";
				var token = $('#token_correo_escribenos').val();
				var element = {
					nombres: $("#nombres_es").val(),
					correo: $("#correo_es").val(),
					telefono: $("#telefono_es").val(),
					descripcion: $("#descripcion_es").val()
				};
				$.ajax({
					url: ruta,
					headers: {'X-CSRF-TOKEN':token},
					type:"POST",
					dataType: 'json',
					data: element,
					success: function(data){ 
						$.notify({message: '¡Gracias por escribirnos!'},{type: 'success'});
						$("#nombres_es").val("");
						$("#correo_es").val("");
						$("#telefono_es").val("");
						$("#descripcion_es").val("");
						$("#preloader").css("display", "none");
						$(".link_scroll_inicio").click();
					},
					error: function(error) {
						console.log(error);
					}             
				});
			}

			function correoTestimonio(){
				if($("#nombres_te").val().trim().length == 0 ||
				$("#correo_te").val().trim().length == 0 ||
				$("#telefono_te").val().trim().length == 0 ||
				$("#descripcion_te").val().trim().length == 0
				){
					$.notify({message: 'Completar los datos correspondientes'},{type: 'danger'});
				}else{
					registrarCorreoTestimonio();
				}
			}
			function registrarCorreoTestimonio(){
				$("#preloader").css("background", "transparent");
				$("#preloader").css("display", "block");

				var ruta = "/principal/ruta_correo_testimonio";
				var token = $('#token_correo_testimonio').val();
				var element = {
					nombres: $("#nombres_te").val(),
					correo: $("#correo_te").val(),
					telefono: $("#telefono_te").val(),
					descripcion: $("#descripcion_te").val()
				};
				$.ajax({
					url: ruta,
					headers: {'X-CSRF-TOKEN':token},
					type:"POST",
					dataType: 'json',
					data: element,
					success: function(data){ 
						$.notify({message: '¡Gracias por escribirnos!'},{type: 'success'});
						$("#nombres_te").val("");
						$("#correo_te").val("");
						$("#telefono_tes").val("");
						$("#descripcion_te").val("");
						$("#preloader").css("display", "none");
						$(".link_scroll_inicio").click();
					},
					error: function(error) {
						console.log(error);
					}             
				});
			}
		</script>
	</body>
</html>
