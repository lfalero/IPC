<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="Luis Alberto Falero Otiniano" name="author">

    <title>{!! config('global.titleSite') !!}</title>

    <link rel="shortcut icon" href="{{ asset('web/assets/image/util/favicon.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('web/assets/image/util/favicon.png') }}">
    
    <link rel="apple-touch-icon-precomposed" sizes="144x144" 
      href="{{ asset('web/assets/image/util/favicon.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" 
      href="{{ asset('web/assets/image/util/favicon.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" 
      href="{{ asset('web/assets/image/util/favicon.png') }}">
    <link rel="apple-touch-icon-precomposed" 
    href="{{ asset('web/assets/image/util/favicon.png') }}">


    <link media="all" type="text/css" rel="stylesheet" href="/administrador/assets/bs/css/bootstrap.min.css">
    
    <link media="all" type="text/css" rel="stylesheet" href="/administrador/assets/bs/css/font-awesome.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="/administrador/assets/build/css/custom.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="/administrador/assets/notify/css/icomoon.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="/administrador/assets/notify/css/titatoggle.min.css">
    
    <script type="text/javascript" src="/administrador/assets/sweetalert2/sweetalert2.min.js"></script>
    <link media="all" type="text/css" rel="stylesheet" href="/administrador/assets/sweetalert2/sweetalert2.min.css">


    <script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>

    <script type="text/javascript" src="/administrador/assets/notify/js/bootstrap-notify.min.js"></script>

    <link media="all" type="text/css" rel="stylesheet" href="/administrador/assets/bs/css/dataTables.bootstrap.min.css">
    <script type="text/javascript" src="/administrador/assets/bs/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/administrador/assets/bs/js/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript" src="/administrador/assets/ckeditor/ckeditor.js"></script>

    <link media="all" type="text/css" rel="stylesheet" href="/administrador/css/base.css">
    <link media="all" type="text/css" rel="stylesheet" href="/administrador/css/spinner.min.css">
    
    <style>.cortar-texto{width: 300px;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;}</style>
     <!-- Other Style -->
    @yield('style') 
  </head>
  
  @if(isset(Auth::user()->dni))
  <body class="nav-md scroll-view-hidden">   

    <div class="preloader">
      <div class="spinner">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>  
    
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col" id="scroll-view-bar">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{URL::to('admin')}}" class="site_title">
                <!--<i class="fa fa-paw"></i> -->
                <span>{!! config('global.subtitleSite') !!}</span>
              </a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
           
              <div class="profile_info">
                <span>{{ 'Bienvenido' }},</span>
                <h2>{{ Auth::user()->nombres .' '. Auth::user()->apellidos}}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br ></script>

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>                   
                  Trabajador
                </h3>
                <ul class="nav side-menu">                   
                  <li>
                    <a href="{{URL::to('admin')}}"><i class="fa fa-home"></i> Inicio</a>
                  </li>
                  <li>
                    <a href="{{URL::to('admin/salmos')}}"><i class="fa fa-book"></i> Salmos</a>
                  </li>
                  <li>
                    <a href="{{URL::to('admin/url-tv-vivos')}}"><i class="fa fa-navicon"></i> URL TV en Vivo</a>
                  </li>
                  <li>
                    <a href="{{URL::to('admin/anuncios')}}"><i class="fa fa-bullhorn"></i> Anuncios</a>
                  </li>
                  <li>
                    <a href="{{URL::to('admin/noticias')}}"><i class="fa fa-bullhorn"></i> Noticias</a>
                  </li>
                  <li>
                    <a href="{{URL::to('admin/testimonios')}}"><i class="fa fa-bullhorn"></i> Testimonios</a>
                  </li>
                  <li>
                    <a href="{{URL::to('admin/imagenes-centrales')}}"><i class="fa fa-image"></i> Imagen Central</a>
                  </li>
                  <li>
                    <a href="{{URL::to('admin/galerias')}}"><i class="fa fa-image"></i> Galería</a>
                  </li>
                  <li>
                    <a href="{{URL::to('admin/locales')}}"><i class="fa fa-map-o"></i> Locales IPC</a>
                  </li>
                  
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <input type="hidden" name="_token" id="token_mostrar_notificaciones" value = "{{ csrf_token() }}">

        <div class="top_nav">
          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" 
                     aria-expanded="false">                    
                    
                    {!! Html::image('images/user-default.png', 
                    Auth::user()->nombres .' '. Auth::user()->apellidos, array('class' => '')) !!}

                    {{ Auth::user()->nombres .' '. Auth::user()->apellidos}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                      <li><a href="{{URL::to('admin/signOff')}}">
                          <i class="fa fa-sign-out pull-right"></i> {{ __('Salir') }}</a>
                      </li>   
                    </li>
                  </ul>
                </li>

                @if(Auth::user()->cargo == 'A'|| Auth::user()->cargo == 'J')
                <li role="presentation" class="dropdown">
                  <a class="dropdown-toggle info-number"
                     data-toggle="dropdown" aria-expanded="true">
                    <i class="fa fa-envelope-o"></i>
                    <span id="info-number">
                        
                    </span>                    
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    
                  </ul>
                </li>
                @endif
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">         
           <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              @include('admin.layouts.alert')
              @yield('content')
            </div>
          </div>
          <br />
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">

          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div> 

    <script type="text/javascript" src="/administrador/assets/bs/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/administrador/assets/build/js/custom.min.js"></script>
    <script type="text/javascript" src="/administrador/js/base.min.js"></script>
    <!-- Other Script -->
    @yield('script') 
   
  </body>
  @else
  <script type="text/javascript">
    window.location="/";
  </script>

  @endif
</html>