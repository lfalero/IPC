@extends('admin.layouts.base')

@section('content')

<!-- top tiles -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel animate">
      <div class="x_title">
        <h2>Listado</h2>       
        <div class="clearfix"></div>
      </div>

      <small class="pull-right">
      	<a href="{{URL::to('admin/url-tv-vivo/nuevo')}}" class="btn btn-info btn-xs">
          <i class="fa fa-bookmark-o"></i> Registrar 
        </a>        
      </small>

      <div class="x_content">   
       
        <div class="row" style="margin-bottom: 40px;">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >
              URL Actual
              <span class="required">*</span>
            </label>
            <div class="col-md-5 col-sm-10 col-xs-12">
              @if(isset($elementoActual))
                {!! Form::select('id', $elementosUrl, $elementoActual["id"], 
                array('id'=>'id', 'class'=>'form-control col-md-7 col-xs-12')); !!} 
              @else                
                {!! Form::select('url', $elementosUrl, null , 
                array('id'=>'id', 'class'=>'form-control col-md-7 col-xs-12')); !!} 
              @endif              
            </div>
            <div class="col-md-2">
                <span>
                  {!! Form::open(['url'=>'/url-tv-vivo/habilitar/', 
                  'class' => 'form-horizontal'] ) !!}
                    <button type="button" class="btn btn-success docs-tooltip" 
                    data-toggle="tooltip" title="" data-original-title="Habilitar" 
                    data-method="Habilitar" title="Habilitar"
                    onclick='
                        generarUrl();
                        var elemento = this;
                        swal({
                          title: "{!! config('global.alertHabilitarTitle') !!}",
                          text: " ",
                          type: "warning",
                          showCancelButton: true,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "Habilitar",
                          cancelButtonText: "Cancelar",
                          closeOnConfirm: false,
                          closeOnCancel: true
                        }).then(function () {                                          
                          elemento.form.submit();                                          
                        });'>
                        <span class="fa fa-refresh"></span>
                    </button>
                  {!! Form::close() !!}
                </span>
            </div>
        </div> 

        <div class="table-responsive">
          <table id="tblListado" class="table table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>                    
                    <th width="40%">Título</th>  
                    <th width="40%">URL</th>  
                    <th width="20%"></th>
                </tr>
            </thead>
            <tbody>   
                @foreach ($elementos as $index => $elementos) 
                  @if($elementos->estado == 'I')            
                    <tr>                
                  @else
                    <tr style="background: #e4e6e8;">                  
                  @endif
                      <td>{!! $elementos->titulo!!}</td>
                      <td>{!! $elementos->url!!}</td>
                      <td> 
                        <center  class="opciones">
                            <div class="btn-group">
                              <a href="{{URL::to('admin/url-tv-vivo/'.$elementos->id)}}">
                                <button type="button" class="btn btn-success docs-tooltip" 
                                data-toggle="tooltip" title="" data-original-title="Modificar" 
                                data-method="Modificar" title="Modificar">
                                    <span class="fa fa-edit"></span>
                                </button>
                              </a>
                              @if($elementos->estado == 'I')
                              <a >
                                {!! Form::open(['url'=>'/url-tv-vivo/eliminar/'.$elementos->id, 
                                'class' => 'form-horizontal'] ) !!}
                                  <button type="button" class="btn btn-danger docs-tooltip" 
                                  data-toggle="tooltip" title="" data-original-title="Eliminar" 
                                  data-method="Eliminar" title="Eliminar"
                                    onclick='
                                        var elemento = this;
                                        swal({
                                          title: "{!! config('global.alertTitle') !!}",
                                          text: " ",
                                          type: "warning",
                                          showCancelButton: true,
                                          confirmButtonColor: "#DD6B55",
                                          confirmButtonText: "Eliminar",
                                          cancelButtonText: "Cancelar",
                                          closeOnConfirm: false,
                                          closeOnCancel: true
                                        }).then(function () {                                          
                                          elemento.form.submit();                                          
                                        });'>
                                      <span class="fa fa-remove"></span>
                                  </button>
                                {!! Form::close() !!}
                              </a>
                              @endif
                            </div>
                        </center>
                      </td> 
                  </tr>     
                @endforeach                               
            </tbody>
          </table>

           <script type="text/javascript">
              $(document).ready(function() {   
                  $('#tblListado').DataTable({
                      order: [[ 0, "asc" ]],
                      pageLength : 25
                  });
              });
          </script>    
        </div>
       
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
  <script>
    function generarUrl(){
      var id = $("#id").val();
      $('form').attr('action', '/url-tv-vivo/habilitar/'+id);
    }
  </script>
@endsection

@section('style')
<style>
  .opciones{
    overflow: hidden;
    width: 100%; 
  }
  .opciones a{
    float: left;
  }

  a{
    margin: 0 5px 0 5px;
  }
</style>
@endsection

