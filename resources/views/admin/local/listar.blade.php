@extends('admin.layouts.base')

@section('content')

<!-- top tiles -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel animate">
      <div class="x_title">
        <h2>Listado</h2>       
        <div class="clearfix"></div>
      </div>

      <small class="pull-right">
      	<a href="{{URL::to('admin/local/nuevo')}}" class="btn btn-info btn-xs">
          <i class="fa fa-bookmark-o"></i> Registrar 
        </a>        
      </small>

      <div class="x_content">   
       
        <div class="table-responsive">
          <table id="tblListado" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>                    
                    <th width="20%">Imagen</th>    
                    <th width="10%">Departamento</th> 
                    <th width="20%">Local</th>  
                    <th width="20%">Días de Culto</th> 
                    <th width="15%">Horario de Culto</th>  
                    <th width="15%"></th>
                </tr>
            </thead>
            <tbody>   
                @foreach ($elementos as $index => $elementos)             
                  <tr>      
                      <td class="text-center">{{ Html::image($elementos->imagen, $elementos->nombre, array('class' => 'image-list')) }}</td>          
                      <td>{!! $elementos->departamento!!}</td>
                      <td>{!! $elementos->nombre!!}</td>
                      <td>{!! $elementos->dias_culto!!}</td>
                      <td>{!! $elementos->horario_culto!!}</td>
                      <td> 
                        <center  class="opciones">
                            <div class="btn-group">
                              <a href="{{URL::to('admin/local/'.$elementos->id)}}">
                                <button type="button" class="btn btn-success docs-tooltip" 
                                data-toggle="tooltip" title="" data-original-title="Modificar" 
                                data-method="Modificar" title="Modificar">
                                    <span class="fa fa-edit"></span>
                                </button>
                              </a>

                              <a >
                                {!! Form::open(['url'=>'/local/eliminar/'.$elementos->id, 
                                'class' => 'form-horizontal'] ) !!}
                                  <button type="button" class="btn btn-danger docs-tooltip" 
                                  data-toggle="tooltip" title="" data-original-title="Eliminar" 
                                  data-method="Eliminar" title="Eliminar"
                                    onclick='
                                        var elemento = this;
                                        swal({
                                          title: "{!! config('global.alertTitle') !!}",
                                          text: " ",
                                          type: "warning",
                                          showCancelButton: true,
                                          confirmButtonColor: "#DD6B55",
                                          confirmButtonText: "Eliminar",
                                          cancelButtonText: "Cancelar",
                                          closeOnConfirm: false,
                                          closeOnCancel: true
                                        }).then(function () {                                          
                                          elemento.form.submit();                                          
                                        });'>
                                      <span class="fa fa-remove"></span>
                                  </button>
                                {!! Form::close() !!}
                              </a>
                            </div>
                        </center>
                      </td> 
                  </tr>     
                @endforeach                               
            </tbody>
          </table>

           <script type="text/javascript">
              $(document).ready(function() {   
                  $('#tblListado').DataTable({
                      order: [[ 0, "asc" ]],
                      pageLength : 25
                  });
              });
          </script>    
        </div>
       
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')

@endsection

@section('style')
<style>
  .opciones{
    overflow: hidden;
    width: 100%; 
  }
  .opciones a{
    float: left;
  }

  a{
    margin: 0 5px 0 5px;
  }
</style>
@endsection

