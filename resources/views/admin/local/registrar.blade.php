@extends('admin.layouts.base')

@section('content')

<!-- top tiles -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel animate">
      <div class="x_title">
        <h2>Formulario</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">     

        @if(isset($elementos))
          {!! Form::open(['url' => '/local/modificar/'.$elementos["id"], 
          'class' => 'form-horizontal form-label-left',
          'enctype'=>'multipart/form-data'] ) !!}
        @else
          {!! Form::open(['url' => '/local/registrar', 
          'class' => 'form-horizontal form-label-left',
          'enctype'=>'multipart/form-data'] ) !!}
        @endif  

        <div class="form-group">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >
              Departamento
              <span class="required">*</span>
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
              @if(isset($elementos))
                {!! Form::select('departamento_id', $elementosDepartamento, $elementos["departamento_id"], 
                array('id'=>'departamento_id', 'class'=>'form-control col-md-7 col-xs-12')); !!} 
              @else                
                {!! Form::select('departamento_id', $elementosDepartamento, null , 
                array('id'=>'departamento_id', 'class'=>'form-control col-md-7 col-xs-12')); !!} 
              @endif              
            </div>
        </div>              
         
        <div class="form-group">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >
            	Nombre
            	<span class="required">*</span>
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
              @if(isset($elementos["nombre"]))
                {!! Form::text('nombre', $elementos["nombre"], 
                array('', 'class'=>'form-control col-md-7 col-xs-12',  
                'placeholder'=>'Nombre')) !!} 
              @else
                {!! Form::text('nombre', null, array('', 'class'=>'form-control col-md-7 col-xs-12',  
                'placeholder'=>'Nombre')) !!} 
              @endif              
            </div>
        </div>
        @if($errors->has('nombre'))
          <div class="form-group" id="content-nombre">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >             
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                  <div class="alert validation" >{{ $errors->first('nombre')  }}</div><br>
                  <script type="text/javascript">
                    $("#content-nombre").addClass("item bad");
                  </script>
            </div>
          </div>            
        @else
          <script type="text/javascript">
            $("#content-nombre").removeClass("item bad");
          </script>
        @endif  

        <div class="form-group">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >
            	Dirección
            	<span class="required">*</span>
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
              @if(isset($elementos["direccion"]))
                {!! Form::text('direccion', $elementos["direccion"], 
                array('', 'class'=>'form-control col-md-7 col-xs-12',  
                'placeholder'=>'Dirección')) !!} 
              @else
                {!! Form::text('direccion', null, array('', 'class'=>'form-control col-md-7 col-xs-12',  
                'placeholder'=>'Dirección')) !!} 
              @endif              
            </div>
        </div>
        @if($errors->has('direccion'))
          <div class="form-group" id="content-direccion">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >             
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                  <div class="alert validation" >{{ $errors->first('direccion')  }}</div><br>
                  <script type="text/javascript">
                    $("#content-direccion").addClass("item bad");
                  </script>
            </div>
          </div>            
        @else
          <script type="text/javascript">
            $("#content-direccion").removeClass("item bad");
          </script>
        @endif

        <div class="form-group">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >
            	Días de Culto
            	<span class="required">*</span>
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
              @if(isset($elementos["dias_culto"]))
                {!! Form::text('dias_culto', $elementos["dias_culto"], 
                array('', 'class'=>'form-control col-md-7 col-xs-12',  
                'placeholder'=>'Días de Culto')) !!} 
              @else
                {!! Form::text('dias_culto', null, array('', 'class'=>'form-control col-md-7 col-xs-12',  
                'placeholder'=>'Días de Culto')) !!} 
              @endif              
            </div>
        </div>
        @if($errors->has('dias_culto'))
          <div class="form-group" id="content-dias_culto">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >             
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                  <div class="alert validation" >{{ $errors->first('dias_culto')  }}</div><br>
                  <script type="text/javascript">
                    $("#content-dias_culto").addClass("item bad");
                  </script>
            </div>
          </div>            
        @else
          <script type="text/javascript">
            $("#content-dias_culto").removeClass("item bad");
          </script>
        @endif
     
        <div class="form-group">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >
            	Horario de Culto
            	<span class="required">*</span>
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
              @if(isset($elementos["horario_culto"]))
                {!! Form::text('horario_culto', $elementos["horario_culto"], 
                array('', 'class'=>'form-control col-md-7 col-xs-12',  
                'placeholder'=>'Horario de Culto')) !!} 
              @else
                {!! Form::text('horario_culto', null, array('', 'class'=>'form-control col-md-7 col-xs-12',  
                'placeholder'=>'Horario de Culto')) !!} 
              @endif              
            </div>
        </div>
        @if($errors->has('horario_culto'))
          <div class="form-group" id="content-horario_culto">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >             
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                  <div class="alert validation" >{{ $errors->first('horario_culto')  }}</div><br>
                  <script type="text/javascript">
                    $("#content-horario_culto").addClass("item bad");
                  </script>
            </div>
          </div>            
        @else
          <script type="text/javascript">
            $("#content-horario_culto").removeClass("item bad");
          </script>
        @endif

        <div class="form-group">
          <label class="col-md-2 col-sm-2 col-xs-12" for="" >
            Imagen
            @if(!isset($elementos["imagen"]))
            <span class="required">*</span>
            @endif  
          </label>
          <div class="col-md-10 col-sm-10 col-xs-12">
            @if(isset($elementos["imagen"]))
              {!! Form::file('imagen', 
              array('class'=>'form-control col-md-7 col-xs-12', 
              'accept'=>'.jpg, .png, .jpeg')) !!} 
            @else
              {!! Form::file('imagen', 
              array('class'=>'form-control col-md-7 col-xs-12', 
              'accept'=>'.jpg, .png, .jpeg')) !!}
            @endif              
          </div>
        </div>
        @if($errors->has('imagen'))
          <div class="form-group" id="content-imagen">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >             
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                  <div class="alert validation" >{{ $errors->first('imagen')  }}</div><br>
                  <script type="text/javascript">
                    $("#content-imagen").addClass("item bad");
                  </script>
            </div>
          </div>            
        @else
          <script type="text/javascript">
            $("#content-imagen").removeClass("item bad");
          </script>
        @endif

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              @if(isset($elementos["id"]))
                {!!  Form::submit(config('global.botonModificar'),
                array('class'=>'btn btn-success register', 'name'=>'modificar')) !!}
              @else
                {!!  Form::submit(config('global.botonGuardar'),
                array('class'=>'btn btn-success register', 'name'=>'guardar_listar')) !!}
                {!!  Form::submit(config('global.botonGuardarSeguir'),
                array('class'=>'btn btn-success register', 'name'=>'guardar')) !!}
              @endif 
              <a href="{{URL::to('admin/locales')}}" class="btn btn-success register">{{ config('global.botonRetroceder') }}</a>
            </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<!-- Select2 -->
<script type="text/javascript" src="/administrador/assets/select2/select2.full.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $("#departamento_id").select2();
    CKEDITOR.config.height = 150;
    CKEDITOR.config.width = 'auto';    
  });
</script>
@endsection

@section('style')
  <!-- Select2 -->
  <link media="all" type="text/css" rel="stylesheet" href="/administrador/assets/select2/select2.min.css">
@endsection

