@extends('admin.layouts.base')

@section('content')

<!-- top tiles -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel animate">
      <div class="x_title">
        <h2>Formulario</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">     

        @if(isset($elementos))
          {!! Form::open(['url' => '/imagen-central/modificar/'.$elementos["id"], 
          'class' => 'form-horizontal form-label-left',
          'enctype'=>'multipart/form-data'] ) !!}
        @else
          {!! Form::open(['url' => '/imagen-central/registrar', 
          'class' => 'form-horizontal form-label-left',
          'enctype'=>'multipart/form-data'] ) !!}
        @endif  

        <div class="form-group">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >
              Categoría
              <span class="required">*</span>
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
              @if(isset($elementos))
                {!! Form::select('categoria_imagen_central_id', $elementosCategoriaImagenCentral, $elementos["categoria_imagen_central_id"], 
                array('id'=>'categoria_imagen_central_id', 'class'=>'form-control col-md-7 col-xs-12')); !!} 
              @else                
                {!! Form::select('categoria_imagen_central_id', $elementosCategoriaImagenCentral, null , 
                array('id'=>'categoria_imagen_central_id', 'class'=>'form-control col-md-7 col-xs-12')); !!} 
              @endif              
            </div>
        </div>              
         
        <div class="form-group">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >
            	Título
            	<span class="required">*</span>
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
              @if(isset($elementos["titulo"]))
                {!! Form::text('titulo', $elementos["titulo"], 
                array('', 'class'=>'form-control col-md-7 col-xs-12',  
                'placeholder'=>'Título')) !!} 
              @else
                {!! Form::text('titulo', null, array('', 'class'=>'form-control col-md-7 col-xs-12',  
                'placeholder'=>'Título')) !!} 
              @endif              
            </div>
        </div>
        @if($errors->has('titulo'))
          <div class="form-group" id="content-titulo">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >             
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                  <div class="alert validation" >{{ $errors->first('titulo')  }}</div><br>
                  <script type="text/javascript">
                    $("#content-titulo").addClass("item bad");
                  </script>
            </div>
          </div>            
        @else
          <script type="text/javascript">
            $("#content-titulo").removeClass("item bad");
          </script>
        @endif  

        <div class="form-group">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >
              Descripción
              <span class="required">*</span>
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
              @if(isset($elementos["descripcion"]))
                {!! Form::textarea('descripcion', $elementos["descripcion"], 
                array('', 'class'=>'form-control col-md-7 col-xs-12 ckeditor')) !!} 
              @else
                {!! Form::textarea('descripcion', null,
                array('', 'class'=>'form-control col-md-7 col-xs-12 ckeditor')) !!} 
              @endif              
            </div>
        </div>
        @if($errors->has('descripcion'))
          <div class="form-group" id="content-descripcion">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >             
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                  <div class="alert validation" >{{ $errors->first('descripcion')  }}</div><br>
                  <script type="text/javascript">
                    $("#content-descripcion").addClass("item bad");
                  </script>
            </div>
          </div>            
        @else
          <script type="text/javascript">
            $("#content-descripcion").removeClass("item bad");
          </script>
        @endif 

        <div class="form-group">
          <label class="col-md-2 col-sm-2 col-xs-12" for="" >
            Imagen
            @if(!isset($elementos["imagen"]))
            <span class="required">*</span>
            @endif              
          </label>
          <div class="col-md-10 col-sm-10 col-xs-12">
            @if(isset($elementos["imagen"]))
              {!! Form::file('imagen', 
              array('class'=>'form-control col-md-7 col-xs-12', 
              'accept'=>'.jpg, .png, .jpeg')) !!} 
            @else
              {!! Form::file('imagen', 
              array('class'=>'form-control col-md-7 col-xs-12', 
              'accept'=>'.jpg, .png, .jpeg')) !!}
            @endif              
          </div>
        </div>
        @if($errors->has('imagen'))
          <div class="form-group" id="content-imagen">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >             
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                  <div class="alert validation" >{{ $errors->first('imagen')  }}</div><br>
                  <script type="text/javascript">
                    $("#content-imagen").addClass("item bad");
                  </script>
            </div>
          </div>            
        @else
          <script type="text/javascript">
            $("#content-imagen").removeClass("item bad");
          </script>
        @endif

        <div class="form-group">
          <label class="col-md-2 col-sm-2 col-xs-12" for="" >
            Nombre Botón
          </label>
          <div class="col-md-10 col-sm-10 col-xs-12">
            @if(isset($elementos["nombre_boton"]))
              {!! Form::text('nombre_boton', $elementos["nombre_boton"], 
              array('', 'class'=>'form-control col-md-7 col-xs-12',  
              'placeholder'=>'Nombre Botón')) !!} 
            @else
              {!! Form::text('nombre_boton', null, array('', 'class'=>'form-control col-md-7 col-xs-12',  
              'placeholder'=>'Nombre Botón')) !!} 
            @endif              
          </div>
        </div>

        <div class="form-group">
          <label class="col-md-2 col-sm-2 col-xs-12" for="">
            Ruta Botón
          </label>
          <div class="col-md-10 col-sm-10 col-xs-12">
            @if(isset($elementos["ruta_boton"]))
              {!! Form::text('ruta_boton', $elementos["ruta_boton"], 
              array('', 'class'=>'form-control col-md-7 col-xs-12',  
              'placeholder'=>'Ruta Botón')) !!} 
            @else
              {!! Form::text('ruta_boton', null, array('', 'class'=>'form-control col-md-7 col-xs-12',  
              'placeholder'=>'Ruta Botón')) !!} 
            @endif              
          </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              @if(isset($elementos["id"]))
                {!!  Form::submit(config('global.botonModificar'),
                array('class'=>'btn btn-success register', 'name'=>'modificar')) !!}
              @else
                {!!  Form::submit(config('global.botonGuardar'),
                array('class'=>'btn btn-success register', 'name'=>'guardar_listar')) !!}
                {!!  Form::submit(config('global.botonGuardarSeguir'),
                array('class'=>'btn btn-success register', 'name'=>'guardar')) !!}
              @endif 
              <a href="{{URL::to('admin/imagenes-centrales')}}" class="btn btn-success register">{{ config('global.botonRetroceder') }}</a>
            </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<!-- Select2 -->
<script type="text/javascript" src="/administrador/assets/select2/select2.full.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $("#categoria_imagen_central_id").select2();
    CKEDITOR.config.height = 150;
    CKEDITOR.config.width = 'auto';    
  });
</script>
@endsection

@section('style')
  <!-- Select2 -->
  <link media="all" type="text/css" rel="stylesheet" href="/administrador/assets/select2/select2.min.css">
@endsection

