@extends('admin.layouts.base')

@section('content')

<!-- top tiles -->
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel animate">
      <div class="x_title">
        <h2>Formulario</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">     

        @if(isset($elementos))
          {!! Form::open(['url' => '/salmo/modificar/'.$elementos["id"], 
          'class' => 'form-horizontal form-label-left',
          'enctype'=>'multipart/form-data'] ) !!}
        @else
          {!! Form::open(['url' => '/salmo/registrar', 
          'class' => 'form-horizontal form-label-left',
          'enctype'=>'multipart/form-data'] ) !!}
        @endif  
             
         
        <div class="form-group">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >
            	Nombre
            	<span class="required">*</span>
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
              @if(isset($elementos["nombre"]))
                {!! Form::textarea('nombre', $elementos["nombre"], 
                array('', 'class'=>'form-control col-md-7 col-xs-12',  
                'placeholder'=>'Nombre', 'rows'=>'4')) !!} 
              @else
                {!! Form::textarea('nombre', null, array('', 'class'=>'form-control col-md-7 col-xs-12',  
                'placeholder'=>'Nombre', 'rows'=>'4')) !!} 
              @endif              
            </div>
        </div>
        @if($errors->has('nombre'))
          <div class="form-group" id="content-nombre">
            <label class="col-md-2 col-sm-2 col-xs-12" for="" >             
            </label>
            <div class="col-md-10 col-sm-10 col-xs-12">
                  <div class="alert validation" >{{ $errors->first('nombre')  }}</div><br>
                  <script type="text/javascript">
                    $("#content-nombre").addClass("item bad");
                  </script>
            </div>
          </div>            
        @else
          <script type="text/javascript">
            $("#content-nombre").removeClass("item bad");
          </script>
        @endif  

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              @if(isset($elementos["id"]))
                {!!  Form::submit(config('global.botonModificar'),
                array('class'=>'btn btn-success register', 'name'=>'modificar')) !!}
              @else
                {!!  Form::submit(config('global.botonGuardar'),
                array('class'=>'btn btn-success register', 'name'=>'guardar_listar')) !!}
                {!!  Form::submit(config('global.botonGuardarSeguir'),
                array('class'=>'btn btn-success register', 'name'=>'guardar')) !!}
              @endif 
              <a href="{{URL::to('admin/salmos')}}" class="btn btn-success register">{{ config('global.botonRetroceder') }}</a>
            </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<!-- Select2 -->
<script type="text/javascript" src="/administrador/assets/select2/select2.full.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $("#categoria_salmo_id").select2();
    CKEDITOR.config.height = 150;
    CKEDITOR.config.width = 'auto';    
  });
</script>
@endsection

@section('style')
  <!-- Select2 -->
  <link media="all" type="text/css" rel="stylesheet" href="/administrador/assets/select2/select2.min.css">
@endsection

