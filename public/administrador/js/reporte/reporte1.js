var f = new Date();
var ff = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
var fff = f.getFullYear() + "-" + valor0(f.getMonth() +1) + "-" + valor0(f.getDate());

var nf = restarDias(f, 30);
var fi = nf.getDate() + "/" + (nf.getMonth() +1) + "/" + nf.getFullYear();	
var ffi = nf.getFullYear() + "-" + valor0(nf.getMonth() +1) + "-" + valor0(nf.getDate());

$(document).ready(function() {
	carga_inicial();

	$("#tipo_pedido_id").change(function() {
		var fi = $('#periodo').data('daterangepicker').startDate.format('YYYY-MM-DD');
		var ff = $('#periodo').data('daterangepicker').endDate.format('YYYY-MM-DD');
	  mostrarReporte(fi, ff);
	});

	$("#exportar_pdf").click(function() {
		var fi = $('#periodo').data('daterangepicker').startDate.format('YYYY-MM-DD');
		var ff = $('#periodo').data('daterangepicker').endDate.format('YYYY-MM-DD');
		var tipo_pedido_id = parseInt($("#tipo_pedido_id").val());
	  $("#fi2").val(fi);
	  $("#ff2").val(ff);
	  $("#tipo_pedido_id2").val(tipo_pedido_id);
	  $('#form_reporte').submit(); 
	});
});

function mostrarReporte(fi, ff){
	var tipo_pedido_id = parseInt($("#tipo_pedido_id").val());
	var ruta = "/reporte/ruta_mostrar_reporte_1";
	var token = $('#token_mostrar_reporte').val();
	var estado = 0;
	var element = {tipo_pedido_id:tipo_pedido_id, fi:fi, ff:ff, estado:estado};

	$(".carga-datos").addClass("hide");
	$(".fa-spinner").removeClass("hide"); 

	$.ajax({
		url: ruta,
		headers: {'X-CSRF-TOKEN':token},
		type:"POST",
		dataType: 'json',
		data: element,
		success: function(data){   
		  
		  if(data.mensaje){ 
				var html = '';
				$('#tblListado').DataTable().clear().destroy();

				if(data.cantidad > 0){
					for(var i=0; i<data.cantidad; i++){

						if(data.elementos[i].estado_formato){
							html +=
			        '<tr>'+
			          '<th scope="row" align="center" width="15%"> '+data.elementos[i]["dia_mes_anio"]+' </th>'+
			          '<td align="center" width="10%"></td>'+
			          '<td align="center" width="10%"></td>'+
			          '<td align="center" width="55%"></td>'+
			          '<td align="center" width="10%"></td>'+
			        '</tr>';
						}else{
							html +=
			        '<tr>'+
			          '<td align="center" width="15%"> '+data.elementos[i]["fecha_entrega"]+' </td>'+
			          '<td width="10%">'+data.elementos[i]["codigo_pedido"]+'</td>'+
			          '<td align=left width="10%">'+data.elementos[i]["tipo"]+'</td>'+
			          '<td align=left width="55%">'+data.elementos[i]["descripcion"]+'</td>'+
			          '<td align=right width="10%">'+data.elementos[i]["monto_cantidad"]+'</td>'+
			        '</tr>';	
						}
						            	
						$("#tbody_reporte").html(html);
					}
				}else{
			  	$("#tbody_reporte").html('')
			  }

			  if ( $.fn.dataTable.isDataTable('#tblListado') ) {
					$('#tblListado').DataTable();
				}
				else {					
					$('#tblListado').DataTable( {
					    //order: [[ 0, "asc" ]],
					    order: [],
					    pageLength : 50
					});
				}
		  }
		},
		error: function(error) {
		  console.log(error);
		}             
	}).complete(function() {
    $(".fa-spinner").addClass("hide");  
		$(".carga-datos").removeClass("hide");  
  });
}

function carga_inicial() {
	$("#tipo_pedido_id").select2();

	$('#periodo').daterangepicker({	 	   
	    "autoclose":"true",
	    "startDate": fi,
    	"endDate": ff,
	    "locale": {
	        "format": "DD/MM/YYYY",
	        "separator": " - ",
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "From",
	        "toLabel": "To",
	        "customRangeLabel": "Custom",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Deciembre"
	        ],
	        "firstDay": 1
    	}
	}, function(start, end, label) {
	 	var fi = start.format('YYYY-MM-DD');
	 	var ff = end.format('YYYY-MM-DD');
	 	mostrarReporte(fi, ff);
	});

	mostrarReporte(ffi, fff);
}

function restarDias(fecha, dias){
  fecha.setDate(fecha.getDate() - dias);
  return fecha;
}

function valor0(valor){
	var formato = ("0" + valor).slice (-2);
	return formato;
}
