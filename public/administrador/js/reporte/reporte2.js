var f = new Date();
var ff = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
var fff = f.getFullYear() + "-" + valor0(f.getMonth() +1) + "-" + valor0(f.getDate());

var nf = restarDias(f, 30);
var fi = nf.getDate() + "/" + (nf.getMonth() +1) + "/" + nf.getFullYear();	
var ffi = nf.getFullYear() + "-" + valor0(nf.getMonth() +1) + "-" + valor0(nf.getDate());

$(document).ready(function() {
	carga_inicial();

	$("#exportar_pdf").click(function() {
		var fi = $('#periodo').data('daterangepicker').startDate.format('YYYY-MM-DD');
		var ff = $('#periodo').data('daterangepicker').endDate.format('YYYY-MM-DD');
	  $("#fi2").val(fi);
	  $("#ff2").val(ff);
	  $('#form_reporte').submit(); 
	});
});

function mostrarReporte(fi, ff){
	var ruta = "/reporte/ruta_mostrar_reporte_2";
	var token = $('#token_mostrar_reporte').val();
	var estado = 0;
	var element = {fi:fi, ff:ff, estado:estado};
	$(".carga-datos").addClass("hide");
	$(".fa-spinner").removeClass("hide"); 

	$.ajax({
		url: ruta,
		headers: {'X-CSRF-TOKEN':token},
		type:"POST",
		dataType: 'json',
		data: element,
		success: function(data){   
		  
		  if(data.mensaje){ 
				var html = '';
				$('#tblListado').DataTable().clear().destroy();

				if(data.cantidad > 0){
					for(var i=0; i<data.cantidad; i++){

						if(data.elementos[i].estado_formato){
							html +=
			        '<tr>'+
			          '<th scope="row" align="center" width="15%"> '+data.elementos[i]["dia_mes_anio"]+' </th>'+
			          '<td align="center" width="10%"></td>'+
			          '<td align="center" width="30%"></td>'+
			          '<td align="center" width="15%"></td>'+
			          '<td align="center" width="15%"></td>'+
			          '<td align="center" width="15%"></td>'+
			        '</tr>';
						}else{
							html +=
			        '<tr>'+
			          '<td align="center" width="15%"> '+data.elementos[i]["fecha_entrega"]+' </td>'+
			          '<td width="10%">'+data.elementos[i]["codigo_pedido"]+'</td>'+
			          '<td align=left width="30%">'+data.elementos[i]["descripcion"]+'</td>'+
			          '<td align=right width="15%">'+data.elementos[i]["ambiente"]+'</td>'+
			          '<td align=left width="15%">'+data.elementos[i]["usuario"]+'</td>'+
			          '<td align=right width="15%">'+data.elementos[i]["tiempo"]+'</td>'+
			        '</tr>';	
						}
						            	
						$("#tbody_reporte").html(html);
					}
				}else{
			  	$("#tbody_reporte").html('')
			  }

			  if ( $.fn.dataTable.isDataTable('#tblListado') ) {
					$('#tblListado').DataTable();
				}
				else {					
					$('#tblListado').DataTable( {
					    //order: [[ 0, "asc" ]],
					    order: [],
					    pageLength : 50
					});
				}
		  }
		},
		error: function(error) {
		  console.log(error);
		}             
	}).complete(function() {
    $(".fa-spinner").addClass("hide");  
		$(".carga-datos").removeClass("hide");  
  });
}

function carga_inicial() {
	$("#tipo_pedido_id").select2();

	$('#periodo').daterangepicker({	 	   
	    "autoclose":"true",
	    "startDate": fi,
    	"endDate": ff,
	    "locale": {
	        "format": "DD/MM/YYYY",
	        "separator": " - ",
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "From",
	        "toLabel": "To",
	        "customRangeLabel": "Custom",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Deciembre"
	        ],
	        "firstDay": 1
    	}
	}, function(start, end, label) {
	 	var fi = start.format('YYYY-MM-DD');
	 	var ff = end.format('YYYY-MM-DD');
	 	mostrarReporte(fi, ff);
	});

	mostrarReporte(ffi, fff);
}

function restarDias(fecha, dias){
  fecha.setDate(fecha.getDate() - dias);
  return fecha;
}

function valor0(valor){
	var formato = ("0" + valor).slice (-2);
	return formato;
}
