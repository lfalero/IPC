$(document).ready(function() {
	"use strict";
	//===============Mobile nav Function============
	var menu = $('#menu');
	var navigation = $('.navigation');
	menu.on('click', function() {
		if ($(window).width() <= 991) {
			navigation.slideToggle('normal');
		}
		return false;
	});
	//===============Submenu Function============
	var navigationLink = $('.navigation>ul> li>a');
	var navigationLi = $('.navigation>ul> li');
	var navigationUl = $('.navigation>ul> li>ul');
	navigationLink.on('click', function() {
		if ($(window).width() <= 991) {
			navigationLi.removeClass('on');
			navigationUl.slideUp('normal');
			if ($(this).next().next('ul').is(':hidden') == true) {
				$(this).parent('li').addClass('on');
				$(this).next().next('ul').slideDown('normal');
			}
		}
		//return false;
	});
	//===============Index1 Mobile nav Function============
	var menu_toggler = $('#menu_toggler');
	var mobile__nav = $('.mobile__nav');
	menu_toggler.on('click', function() {
		if ($(window).width() <= 991) {
			mobile__nav.slideToggle('normal');
		}
		return false;
	});
	//===============Submenu Function============
	var mobile__navLink = $('.mobile__nav>ul> li>a');
	var mobile__navLi = $('.mobile__nav>ul> li');
	var mobile__navUl = $('.mobile__nav>ul> li>ul');
	mobile__navLink.on('click', function() {
		if ($(window).width() <= 991) {
			mobile__navLi.removeClass('on');
			mobile__navUl.slideUp('normal');
			if ($(this).next().next('ul').is(':hidden') == true) {
				$(this).parent('li').addClass('on');
				$(this).next().next('ul').slideDown('normal');
			}
		}
		//return false;
	});

	/*----------------------------------------------
	 -----------Masonry Function  --------------------
	 -------------------------------------------------*/
	var container_masonry = $(".container-masonry");
	container_masonry.imagesLoaded(function() {
		container_masonry.isotope({
			itemSelector : '.nf-item',
			transitionDuration : '1s',
			percentPosition : true,
			masonry : {
				columnWidth : '.grid-sizer'
			}
		});
	});

	/*----------------------------------------------
	 -----------Masonry filter Function  --------------------
	 -------------------------------------------------*/
	var container_filter = $(".container-filter");
	container_filter.on("click", ".categories", function() {
		var a = $(this).attr("data-filter");
		container_masonry.isotope({
			filter : a
		});

	});
	/*----------------------------------------------
	 -----------Masonry filter Active Function  --------------------
	 -------------------------------------------------*/
	container_filter.each(function(e, a) {
		var i = $(a);
		i.on("click", ".categories", function() {
			i.find(".active").removeClass("active"), $(this).addClass("active");
		});
	});

	/*----------------------------------------------
	 -----------Masonry Grid view Function  --------------------
	 -------------------------------------------------*/
	var container_grid = $(".container-grid");
	container_grid.imagesLoaded(function() {
		container_grid.isotope({
			itemSelector : ".nf-item",
			layoutMode : "fitRows"
		});
	});

	/*----------------------------------------------
	 -----------Masonry Grid Filter Function  --------------------
	 -------------------------------------------------*/
	container_filter.on("click", ".categories", function() {
		var e = $(this).attr("data-filter");
		container_grid.isotope({
			filter : e
		});
	});

	/*----------------------------------------------
	 -----------isotope Function  --------------------
	 -------------------------------------------------*/
	var isotop_grid = $('#isotope');
	if (isotop_grid.length) {
		// init Isotope
		var $grid = isotop_grid.isotope({
			itemSelector : 'li	',
			percentPosition : true,
			layoutMode : 'fitRows',
			fitRows : {
				gutter : 0
			}
		});
	}
	/*----------------------------------------------
	 -----------Light Function  --------------------
	 -------------------------------------------------*/
	var fLight = $(".fancylight");
	if (fLight.length) {
		fLight.fancybox({
			openEffect : 'elastic',
			closeEffect : 'elastic',
			helpers : {
				media : {}
			}
		});
	}

	(function($) {
		"use strict";
		if ($("a[rel^='prettyPhoto'], a[data-rel^='prettyPhoto']").length != 0) {
			$("a[rel^='prettyPhoto'], a[data-rel^='prettyPhoto']").prettyPhoto({
				hook : 'data-rel',
				theme : "dark_square",
				social_tools : false,
				deeplinking : false
			});
		}
	})(jQuery);

	//Services
	var related_project = $("#related-project");
	related_project.owlCarousel({
		loop : true,
		nav : true,
		dots : false,
		margin : 30,
		responsive : {
			0 : {
				items : 1
			},
			767 : {
				items : 2
			},
			992 : {
				items : 2
			},
			1200 : {
				items : 3
			}
		},
		navText : ["<i class='ion-ios-arrow-back'></i>", "<i class='ion-ios-arrow-forward'></i>"]

	});
	var owl_demo = $("#owl-demo");
	owl_demo.owlCarousel({
		navigation : true, // Show next and prev buttons
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem : true
	});
	/*----------------------------------------------
	 -----------Counter Function  --------------------
	 -------------------------------------------------*/
	var counter = $('.counter');
	if (counter.length) {
		counter.appear(function() {
			counter.each(function() {
				var e = $(this),
				    a = e.attr("data-count");
				$({
					countNum : e.text()
				}).animate({
					countNum : a
				}, {
					duration : 8e3,
					easing : "linear",
					step : function() {
						e.text(Math.floor(this.countNum));
					},
					complete : function() {
						e.text(this.countNum);
					}
				});
			});
		});
	}
	//	Releted Project slider
	var relatedProject = $("#related-project");
	relatedProject.owlCarousel({
		loop : true,
		nav : true,
		dots : false,
		margin : 30,
		responsive : {
			0 : {
				items : 1
			},
			767 : {
				items : 2
			},
			992 : {
				items : 2
			},
			1200 : {
				items : 3
			}
		},
		navText : ["<i class='ion-ios-arrow-back'></i>", "<i class='ion-ios-arrow-forward'></i>"]

	});

	//team-Carosel
	var team_carousel = $('#team-carousel');
	if (team_carousel.length) {
		team_carousel.owlCarousel({
			loop : true,
			margin : 30,
			nav : false,
			dots : false,
			navText : ["<i class='icon-arrow-left'></i>", "<i class='icon-arrow-right'></i>"],
			responsive : {
				0 : {
					items : 1
				},
				767 : {
					items : 2
				},
				992 : {
					items : 4
				},
				1200 : {
					items : 4
				}
			}
		});
	}

	//	Testimonial-2 Carousel
	var testimonial_2 = $('.testimonial-carousel');
	testimonial_2.owlCarousel({
		loop : true,
		margin : 10,
		nav : false,
		autoplay : true,
		autoplayTimeout : 5000,
		dots : true,
		navText : ["<i class='ion-ios-arrow-back'></i>", "<i class='ion-ios-arrow-forward'></i>"],
		responsive : {
			0 : {
				items : 1
			},
			767 : {
				items : 1
			},
			992 : {
				items : 1
			},
			1200 : {
				items : 1
			}
		}
	});

	//	Blog
	var testimonial_2 = $('.blog-carousel');
	testimonial_2.owlCarousel({
		loop : true,
		margin : 10,
		dots : true,
		nav : false,
		autoplay : true,
		autoplayTimeout : 3000,
		dots : true,
		navText : ["<i class='ion-ios-arrow-back'></i>", "<i class='ion-ios-arrow-forward'></i>"],
		responsive : {
			0 : {
				items : 1
			},
			767 : {
				items : 1
			},
			992 : {
				items : 1
			},
			1200 : {
				items : 1
			}
		}
	});

	var noticiasCarousel = $('.noticias-carousel');
	noticiasCarousel.owlCarousel({
		loop : true,
		margin : 10,
		dots : true,
		nav : false,
		autoplay : true,
		autoplayTimeout : 6000,
		dots : true,
		navText : ["<i class='ion-ios-arrow-back'></i>", "<i class='ion-ios-arrow-forward'></i>"],
		responsive : {
			0 : {
				items : 1
			},
			400 : {
				items : 2
			},
			992 : {
				items : 3
			},
			1200 : {
				items : 3
			}
		}
	});
	
	var anunciosCarousel = $('.anuncios-carousel');
	anunciosCarousel.owlCarousel({
		loop : true,
		margin : 10,
		dots : true,
		nav : false,
		autoplay : true,
		autoplayTimeout : 6000,
		dots : true,
		navText : ["<i class='ion-ios-arrow-back'></i>", "<i class='ion-ios-arrow-forward'></i>"],
		responsive : {
			0 : {
				items : 1
			},
			400 : {
				items : 2
			},
			992 : {
				items : 3
			},
			1200 : {
				items : 3
			}
		}
	});

	var anunciosCarousel = $('.testimonio-carousel');
	anunciosCarousel.owlCarousel({
		loop : true,
		margin : 10,
		dots : true,
		nav : false,
		autoplay : true,
		autoplayTimeout : 6000,
		dots : true,
		navText : ["<i class='ion-ios-arrow-back'></i>", "<i class='ion-ios-arrow-forward'></i>"],
		responsive : {
			0 : {
				items : 1
			},
			400 : {
				items : 2
			},
			992 : {
				items : 3
			},
			1200 : {
				items : 3
			}
		}
	});

	/*accordion*/
	var accordion_select = $('.accordion');
	if (accordion_select) {
		accordion_select.each(function() {
			$(this).accordion({
				"transitionSpeed" : 400,
				transitionEasing : 'ease-in-out'
			});
		});
	}

	//Client
	var client_carousel = $('.client-carousel');
	if (client_carousel.length) {
		client_carousel.owlCarousel({
			loop : true,
			margin : 10,
			nav : true,
			dots : false,

			navText : ["<i class='ion-ios-arrow-back'></i>", "<i class='ion-ios-arrow-forward'></i>"],
			responsive : {
				0 : {
					items : 1
				},
				767 : {
					items : 3
				},
				992 : {
					items : 5
				},
				1200 : {
					items : 5
				}
			}
		});
	}

	var btt = $('#back-to-top');
	$(window).on('scroll', function() {
		if ($(this).scrollTop() > 50) {
			btt.fadeIn();
		} else {
			btt.fadeOut();
		}
	});
	// scroll body to 0px on click
	btt.on('click', function() {
		btt.tooltip('hide');
		$('body,html').animate({
			scrollTop : 0
		}, 800);
		return false;
	});

	//window load
	$(window).on('load', function() {
		var preloader = $('#preloader');
		preloader.delay().fadeOut();

	});

	// End
});
